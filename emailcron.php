<?php
//include_once 'public/PHPMailer/src/PHPMailer.php';
//include_once 'public/PHPMailer/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

$mysqli = new mysqli("localhost", "anujdala_shopify", "5bSdT0rmiU*a", "anujdala_multiple_custom_forms");
$select_cron_emails_query = "Select * from email_records where email_status = 0 limit 10";
if ($res = $mysqli->query($select_cron_emails_query)) {
    if ($res->num_rows > 0) {
        while ($row = $res->fetch_assoc()) {
            $filename = basename($row['attachment_path']);
            $file = $row['attachment_path'];

            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'rusertesting@gmail.com';           // SMTP username
                $mail->Password = 'rtest123456';                      // SMTP password
                $mail->SMTPSecure = 'SSL';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;                                    // TCP port to connect to

                //Recipients
                $mail->setFrom($row['sender'], 'Ritesh');
                $mail->addAddress($row['receiver'], 'Chandra Prakash');     // Add a recipient
                $mail->addReplyTo($row['sender'], 'Ritesh');

                //Attachments
                $mail->addAttachment($file);         // Add attachments

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = $row['subject'];
                $mail->Body    = $row['email_template'];
                $mail->send();
                echo 'Message has been sent';
            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }
        }
    }
}
