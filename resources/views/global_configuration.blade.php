@extends('header')
@section('content')
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Gobal Configuration',
            buttons: {
                secondary: [{
                        label: 'Help',
                        href: '{{ url('help') }}',
                        loading: true
                    }]
            }
        });
    });
</script>

    <div class="col-md-8 col-xs-12 panel">
        <form action="{{ url('gobal_configuration_save') }}" method="post" id="configform">
            {{ csrf_field() }}
            <button type="submit" href="<?php /* echo ''; */ ?>" value="Save Config" class="btn btn-primary CreateDashboardbtn" style="float: right;width: 150px;display: block;vertical-align: middle;height: 40px;margin-top: 10px;line-height: 1em;">
                <span class="glyphicon glyphicon-plus"></span> save Config
            </button>
            <div class="main-col-inner">
                <!--<h1 class="panel-heading" style="font-size: 24px;width: 100%;">Multiple Forms Configuration</h1>-->
                <div class="entry-edit">
                    <div class="section-config panel-body">
                        <legend class="info-head">Date &amp; Time Options</legend>
                        <table class="table table-inverse">
                            <colgroup></colgroup>
                            <colgroup class="value"></colgroup>
                            <tbody>
                                
                                <input id="use_calendar" name="use_jscalendar" value ="1" type = "hidden"/>
                                
<!--                                <tr>
                                    <td>
                                        <label for="use_calendar">Use JavaScript Calendar</label>
                                    </td>
                                    <td class="value">
                                        <select id="use_calendar" name="use_jscalendar">
                                            <option <?php /*echo ($data != NULL && $data->use_jscalendar) ? 'selected': '';*/ ?> value="1">Yes</option>
                                            <option <?php // echo ($data != NULL && !$data->use_jscalendar) ? 'selected': ''; ?> value="0">No</option>
                                        </select>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td>
                                        <label for="date_field_order"> Date Fields Order</label>
                                    </td>
                                    <td class="value" id="date_field_order">
                                        <?php $date_field_order = ($data != NULL && $data->date_field_order != '') ? unserialize($data->date_field_order) : array(); ?>
                                        <select name="date_field_order[0]">
                                            <option <?php echo ($data != NULL && $date_field_order[0] == 'd') ? 'selected': ''; ?> value="d">Day</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[0] == 'm') ? 'selected': ''; ?> value="m">Month</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[0] == 'y') ? 'selected': ''; ?> value="y">Year</option>
                                        </select>
                                        <span>/</span>
                                        <select name="date_field_order[1]">
                                            <option <?php echo ($data != NULL && $date_field_order[1] == 'd') ? 'selected': ''; ?> value="d">Day</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[1] == 'm') ? 'selected': ''; ?> value="m">Month</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[1] == 'y') ? 'selected': ''; ?> value="y">Year</option>
                                        </select>
                                        <span>/</span>
                                        <select name="date_field_order[2]">
                                            <option <?php echo ($data != NULL && $date_field_order[2] == 'd') ? 'selected': ''; ?> value="d">Day</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[2] == 'm') ? 'selected': ''; ?> value="m">Month</option>
                                            <option  <?php echo ($data != NULL && $date_field_order[2] == 'y') ? 'selected': ''; ?> value="y">Year</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="time_format"> Time Format</label>
                                    </td>
                                    <td class="value">
                                        <select id="atime_format" name="time_format">
                                            <option <?php echo ($data != NULL && $data->time_format == '1') ? 'selected' : ''; ?> value="1">12h AM/PM</option>
                                            <option <?php echo ($data != NULL && $data->time_format == '2') ? 'selected' : ''; ?> value="2">24h</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="year_range"> Year Range</label>
                                    </td>
                                    <td class="value" id="year_range">
                                        from <input id="year_range_from" name="year_range_from" value="<?php echo ($data != NULL && $data->year_range != '') ? explode('-', $data->year_range)[0] : ''; ?>" type="text">
                                        to <input id="year_range_to" name="year_range_to" value="<?php echo ($data != NULL && $data->year_range != '') ? explode('-', $data->year_range)[1] : ''; ?>" type="text">
                                        <p class="note"><span>Use four-digit year format.</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="section-config active panel-body">
                        <legend  class="info-head">Google reCAPTCHA</legend>
                        <table cellspacing="0" class="table table-inverse">
                            <colgroup></colgroup>
                            <colgroup class="value"></colgroup>
                            <colgroup class="scope-label"></colgroup>
                            <colgroup class=""></colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="enable_captcha"> Enabled</label>
                                    </td>
                                    <td class="value">
                                        <select id="enable_captcha" name="enable_captcha">
                                            <option <?php echo ($data != NULL && $data->enable_captcha) ? 'selected' : ''; ?> value="1">Yes</option>
                                            <option <?php echo ($data != NULL && !$data->enable_captcha) ? 'selected' : ''; ?> value="0">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="site_key"> Site Key</label>
                                    </td>
                                    <td class="value">
                                        <input id="site_key" name="site_key" value="<?php echo ($data != NULL && $data->site_key != '') ? $data->site_key : '' ?>" class="input-text" type="text">
                                        <p class="note"><span><a href="https://www.google.com/recaptcha/admin" target="_blank">Create a site key</a></span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="secret_key"> Secret Key</label>
                                    </td>
                                    <td class="value">
                                        <input id="secret_key" name="secret_key" value="<?php echo ($data != NULL && $data->secret_key != '') ? $data->secret_key : '' ?>" class="input-text" type="text">
                                        <p class="note"><span><a href="https://www.google.com/recaptcha/admin" target="_blank">Create a secret key</a></span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="theme"> Theme</label>
                                    </td>
                                    <td class="value">
                                        <select id="theme" name="theme">
                                            <option <?php echo ($data != NULL && $data->theme == 'light') ? 'selected' : ''; ?> value="light">Light</option>
                                            <option <?php echo ($data != NULL && $data->theme == 'dark') ? 'selected' : ''; ?> value="dark">Dark</option>
                                        </select>
                                        <p class="note"><span>See the Google reCAPTCHA Documentation <a href="https://developers.google.com/recaptcha/docs/display#render_param" target="_blank">for more</a></span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="type"> Type</label></td>
                                    <td class="value">
                                        <select id="type" name="type">
                                            <option  <?php echo ($data != NULL && $data->type == '1') ? 'selected' : ''; ?> value="1">Image</option>
                                            <option  <?php echo ($data != NULL && $data->type == '2') ? 'selected' : ''; ?> value="2">Audio</option>
                                        </select>
                                        <p class="note"><span>See the Google reCAPTCHA Documentation <a href="https://developers.google.com/recaptcha/docs/display#render_param" target="_blank">for more</a></span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="size"> Size</label></td>
                                    <td class="value">
                                        <select id="size" name="size">
                                            <option  <?php echo ($data != NULL && $data->size == '1') ? 'selected' : ''; ?> value="1">Normal</option>
                                            <option  <?php echo ($data != NULL && $data->size == '2') ? 'selected' : ''; ?> value="2">Compact</option>
                                        </select>
                                        <p class="note"><span>See the Google reCAPTCHA Documentation <a href="https://developers.google.com/recaptcha/docs/display#render_param" target="_blank">for more</a></span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection