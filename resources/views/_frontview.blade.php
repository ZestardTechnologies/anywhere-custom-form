<div id="zestard-anywhere-form-box" class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 form-title-text"><h3><?php echo $form['form_name']; ?></h3></div>
    <div class="col-xs-12 col-sm-12 col-md-8">
        <form method="post" action="<?php echo url('send_email'); ?>" id="<?php echo 'multiform' . $form['form_id']; ?>" class="form-horizontal" 
              enctype="multipart/form-data" onsubmit="return validatemultiform(this);">

            <input type="hidden" name="form_id" value="<?php echo $form['form_id']; ?>" />
            <input type="hidden" class="sender" name="sender" value="" />
            <?php
            $data = array();
            $count = 0;
            foreach ($fields as $field) {
                echo '<div class="anywhere-custom-form-group">';
                $field['count'] = $count;
                $field_type = $field['type'];
                $type = 'type.' . $field['type'];
                $row = (object) $field;
                if (View::exists($type)) {
                    echo view($type)->with(['row' => $row, 'config' => $config])->render();
                }
                $count++;
                echo '</div>';
            }
            if ($config->enable_captcha) {
                $data['form'] = $form;
                echo '<div class="form-group">';
                echo view('type.recaptcha')->with(['form' => $data, 'config' => $config])->render();
                echo '</div>';
            }
            ?>
            <div class="form-group">
                <div class="col-sm-offset-11 col-sm-1">
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
                <div>
                </div>
                <div>
                    <script type="text/javascript">
                        function validatemultiform(data) {
                            $('.sender').val($('input[type=email]').val());
                            var required = 0;
                            $('.required-entry').css('border-color', '#ccc');
                            $('.validation-advice').remove();
                            $('.required-entry').each(function(){                                
                                if ($(this).val() == '' || $(this).val() == null ) {                                                       if ($(this).hasClass('selection')) {                                        
                                        if (!$(this).children().find('.option').is(':checked')) {
                                            
                                            $(this).css('border-color', '#df280a');
                                            $(this).append('<div class="validation-advice">This is a required field.</div>');
                                            required += 1;                                            
                                        }
                                    } //else if ($(this).val() == '' || $(this).val() == null) {
                                    else {                                        
                                        $(this).css('border-color', '#df280a');
//                                        if ($(this).context.type == 'select-one') {
//                                            $(this).next('.validation-advice').remove();
//                                        }                        
                                        $(this).append('<div class="validation-advice">This is a required field.</div>');                               
                                        required += 1;                                        
                                    }
                                }
                                else
                                {
                                    $(this).css('border-color', '#000');
                                }
                            });                            
                            var count = 0;
                            $('.requireddatetime').each(function () {
                                count++;
                                var temp = 0;
                                var $this = $(this);
                                //console.log($this.children('.required'));
                                $this.children('.required').each(function () {
                                    if (count == 3) {
                                        console.log($(this).val() == '' || $(this).val() == null);
                                    }

                                    if ($(this).val() == '' || $(this).val() == null) {
                                        temp += 1;
                                    }
                                });
                                if (temp > 0) {
                                    $(this).closest('.form-group').children('.requireddatetime').append('<div class="validation-advice">This is a required field.</div>');
                                }
                            });
                            //alert(required);    
                            //console.log(required);
                            if (required) {
                                return false;
                            }
                            startloader(1);
                            return true;
                        }
                    </script>
                    </body>
                    </html>
                    <style>
                        #zestard-anywhere-form-box{
                            border: 1px solid black;
                            max-width: 70%;
                            margin-left: 15%;
                            padding: 20px;

                        }
                        .form-title-text{
                            text-align: center;
                            margin-bottom: 20px;
                            line-height: 3em;
                        }
                        .anywhere-custom-form-group{
                            display: block;
                            width: 100%;
                        }
                        .anywhere-custom-label{
                            float: left;
                            cursor: pointer;
                            vertical-align: central;
                            display: inline-block;
                            margin-bottom: 5px;
                            max-width: 150px;
                            min-width: 140px;
                            margin-right: 20px;
                        }
                        .anywhere-custom-control-box{
                            display: inline-block;
                            min-width: 150px;
                            max-width: 600px;
                            color: #000;
                            max-width: 100%;
                            font-size: 16px;
                            font-family: "Work Sans","HelveticaNeue","Helvetica"; 
                            margin-bottom: 10px;

                        }
                        .anywhere-custom-control{
                            width: 100% !important;
                            /*min-width: 150px;*/

                            border: 1px solid #e8e9eb;
                            border-radius: 5px;
                            background-color: #fff;
                            color: #000;
                        }
                    </style>