<?php
$count = 1;
foreach($Fields as $key => $Field) {
?>
<div class="table-responsive option-box info" id="<?php echo 'field_'.$count; ?>">
   <table id="contact_field" class="option-header table info table-inverse" cellpadding="0" cellspacing="0">
      <thead>
         <tr class="info">
            <th class="opt-title">Title <span class="required">*</span></th>
            <th class="opt-type">Input Type <span class="required">*</span></th>
            <th class="opt-req">Is Required</th>
            <th class="opt-order">
				Sort Order
				<div class="help-tip">
					<p>Enter the numeric value.</p>
				</div>
			</th>
            <th class="a-right"></th>
         </tr>
      </thead>
      <tbody>
         <tr class="info">
            <td>
			   <input type="hidden" name="<?php  echo 'contact[fields]['.$count.'][field_id]'; ?>" class="<?php echo 'fieldid'.$count ?>" value="<?php echo $Field->field_id; ?>">
               <input type="text" class="form-control required-entry" id="<?php echo 'contact_field_'.$count.'_title'; ?>" name="<?php  echo 'contact[fields]['.$count.'][title]'; ?>" value="<?php echo $Field->title; ?>">
            </td>
            <td>
               <select name="<?php  echo 'contact[fields]['.$count.'][type]'; ?>" id="<?php echo 'contact_field_'.$count.'_type'; ?>" onchange="<?php echo 'choosoption(this,'.$count.')'; ?>" class="form-control required-entry" title="">
                  <option value="">-- Please select --</option>
                  <optgroup label="Text">
                     <option <?php echo ($Field->type == 'field')? 'selected':''; ?> value="field">Field</option>
                     <option <?php echo ($Field->type == 'area')? 'selected':''; ?> value="area">Area</option>
                  </optgroup>
                  <optgroup label="File">
                     <option <?php echo ($Field->type == 'file')? 'selected':''; ?> value="file">File</option>
                  </optgroup>
                  <optgroup label="Select">
                     <option <?php echo ($Field->type == 'drop_down')? 'selected':''; ?> value="drop_down">Drop-down</option>
                     <option <?php echo ($Field->type == 'radio')? 'selected':''; ?> value="radio">Radio Buttons</option>
                     <option <?php echo ($Field->type == 'checkbox')? 'selected':''; ?> value="checkbox">Checkbox</option>
                     <option <?php echo ($Field->type == 'multiple')? 'selected':''; ?> value="multiple">Multiple Select</option>
                  </optgroup>
                  <optgroup label="Date">
                     <option <?php echo ($Field->type == 'date')? 'selected':''; ?> value="date">Date</option>
                     <option <?php echo ($Field->type == 'date_time')? 'selected':''; ?> value="date_time">Date &amp; Time</option>
                     <option <?php echo ($Field->type == 'time')? 'selected':''; ?> value="time">Time</option>
                  </optgroup>
               </select>
            </td>
            <td class="opt-req">
				<select name="<?php echo 'contact[fields]['.$count.'][is_require]'; ?>" id="<?php echo 'contact_field_'.$count.'_is_require'; ?>" class="form-control required-entry" title="">
                  <option <?php echo ($Field->is_require) ? 'selected':''; ?> value="1">Yes</option>
                  <option <?php echo (!$Field->is_require)? 'selected':''; ?> value="0">No</option>
               </select>
            </td>
            <td>
			<input type="number" class="form-control required-entry" name="<?php echo 'contact[fields]['.$count.'][sort_order]'; ?>" value="<?php echo ($Field->sort_order) ?>" min="1">
			</td>
            <td>
                <button id="<?php echo 'del_'.$count ?>" title="Delete Option" type="button" class="scalable delete btn btn-danger" onclick="<?php  echo 'deleteField('.$count.');'; ?>" style=""><span><span><span><i class="glyphicon glyphicon-trash"></i></span></span></span></button>
            </td>
         </tr>
      </tbody>
   </table>
	<?php if( $Field->type == 'drop_down' || $Field->type == 'radio' || $Field->type == 'checkbox' || $Field->type == 'multiple'): ?>
	<?php if(isset($Field->child_options) && $Field->child_options != ''): ?>
	<div id="<?php echo 'contact_field_'.$count.'_type_select'; ?>" class="grid col-sm-12 tier form-list">
	   <table class="table border table-inverse" cellpadding="0" cellspacing="0">
		  <thead>
			 <tr class="info headings">
				<th class="type-title">Title<span class="required">*</span></th>
				<!-- <th class="type-order">Sort Order</th> -->
				<th class="type-butt last">&nbsp;</th>
			 </tr>
		  </thead>
		  <tbody id="<?php echo 'select_field_type_row_'.$count; ?>">
			<?php
			$subCount = 1;
			$ChildFields = unserialize($Field->child_options);
			foreach($ChildFields as $key => $option) {
			?>
			<tr class="subchildrows" id="<?php echo 'contact_field_'.$count.'_select_'.$subCount; ?>">
				<td><input type="text" class="form-control required-entry select-type-title" id="<?php echo 'contact_field_'.$count.'_select_'.$subCount.'_title'; ?>" name="<?php echo 'contact[fields]['.$count.'][values]['.$subCount.'][title]'; ?>" value="<?php echo $option['title']; ?>"></td>
				<td class="last"><span title="Delete row"><button id="<?php echo 'id_'.$subCount; ?>" title="Delete Row" type="button" class="scalable delete delete-select-row btn btn-warning icon-btn" onclick="<?php echo 'removeRow('.$subCount.','.$count.')'; ?>" style=""><span><span><span><i class="glyphicon glyphicon-remove"></i></span></span></span></button></span></td>
			</tr>
			<?php $subCount++; } ?>
		  </tbody>
		  <tfoot>
			 <tr class="info">
				<td colspan="100" class="a-right"><button id="<?php echo 'add_select_row_button_'.$count; ?>" title="Add New Row" type="button" class="scalable add add-select-row btn btn-primary" onclick="<?php echo 'addrow('.$count.')'; ?>" style="float:right;"><span><span><span class="glyphicon glyphicon-plus"></span></span></span></button></td>
			 </tr>
		  </tfoot>
	   </table>
	</div>
	<?php endif; ?>
	<?php endif; ?>
	<?php if( $Field->type == 'field' || $Field->type == 'area'): ?>
	<div id="<?php echo 'contact_field_'.$count.'_type_text'; ?>" class="table-responsive  grid tier col-sm-12 form-list">
	   <table class="table border table-inverse" cellpadding="0" cellspacing="0">
		  <tbody>
			 <tr class="info headings">
				<th class="type-validation">Validation</th>
				<th class="type-text">
					Max Characters
					<div class="help-tip">
                        <p>Enter the numeric value.</p>
                    </div>
				</th>
				<th class="type-last last">
                    Set as Email Sender
                    <div class="help-tip">
                        <p>if you select this it will set this field as sender of email.</p>
                    </div>
                </th>
			 </tr>
			 <tr class="info">
				<td>
				   <select name="<?php echo 'contact[fields]['.$count.'][validation]'; ?>" id="<?php echo 'contact_field_'.$count.'_validation'; ?>" class="select form-control" title="">
					  <option <?php echo ($Field->validation == 'no')? 'selected':''; ?> value="no">No</option>
					  <option <?php echo ($Field->validation == 'email')? 'selected':''; ?> value="email">Email</option>
				   </select>
				</td>
				<td class="type-last last"><input type="number" class="required-entry form-control validate-zero-or-greater" name="<?php echo 'contact[fields]['.$count.'][max_characters]'; ?>" value="<?php echo $Field->max_characters; ?>" min="1"></td>
				<td><input type="radio" class="emailsender" name="<?php echo 'contact[fields]['.$count.'][email_sender]'; ?>" <?php echo ($Field->email_sender == 1)? 'checked':''; ?>></td>
			 </tr>
		  </tbody>
	   </table>
	</div>
	<?php endif; ?>
	<?php if( $Field->type == 'file'): ?>
	<div id="<?php echo 'contact_field_'.$count.'_type_file'; ?>" class="table-responsive col-sm-12 grid tier form-list">
	   <table class="table border table-inverse" cellpadding="0" cellspacing="0">
		  <tbody>
			 <tr class="headings info">
				<th class="type-title">
					Allowed File Extensions
					<div class="help-tip">
						<p>Enter the flie extenstion which you want to allow with comma seperated like(jpg,png).</p>
					</div>
				</th>
				<th class="last">
					Maximum File Size
					<div class="help-tip">
                        <p>Enter the numeric value.</p>
                    </div>
				</th>
			 </tr>
			 <tr class="info">
				<td><input class="form-control required-entry" type="text" name="<?php echo 'contact[fields]['.$count.'][file_extension]'; ?>" value="<?php echo $Field->file_extension; ?>"></td>
				<td class="type-last last" nowrap=""><input class="form-control required-entry" type="number" name="<?php echo 'contact[fields]['.$count.'][file_size]'; ?>" value="<?php echo $Field->file_size; ?>" min="1"> MB</td>
			 </tr>
		  </tbody>
	   </table>
	</div>
	<?php endif; ?>
</div>
<?php
$count++;
}
?>
<style>
.help-tip{
    position: relative;
    display: inline-block;
    /* top: 18px; 
    right: 18px; */
    left: 5px;
    text-align: center;
    background-color: black;
    border-radius: 50%;
    width: 15px;
    height: 15px;
    font-size: 14px;
    line-height: 15px;
    cursor: pointer;
}

.help-tip:before{
    content:'i';
    font-weight: bold;
    color:#fff;
}

.help-tip:hover p{
    display:block;
    transform-origin: 100% 0%;

    -webkit-animation: fadeIn 0.3s ease-in-out;
    animation: fadeIn 0.3s ease-in-out;

}

.help-tip p{    /* The tooltip */
    display: none;
    text-align: left;
    background-color: #1E2021;
    padding: 20px;
    width: 300px;
    position: absolute;
    border-radius: 3px;
    box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
    right: auto;
    color: #FFF;
    font-size: 13px;
    line-height: 1.4;
    left: -16px;
    top: 19px;
}

.help-tip p:before{ /* The pointer of the tooltip */
    position: absolute;
    content: '';
    width:0;
    height: 0;
    border:6px solid transparent;
    border-bottom-color:#1E2021;
    left: 16px;
    top: -12px;
}

.help-tip p:after{ /* Prevents the tooltip from being hidden */
    width:100%;
    height:40px;
    content:'';
    position: absolute;
    top:-40px;
    left:0;
}

/* CSS animation */

@-webkit-keyframes fadeIn {
    0% { 
        opacity:0; 
        transform: scale(0.6);
    }

    100% {
        opacity:100%;
        transform: scale(1);
    }
}

@keyframes fadeIn {
    0% { opacity:0; }
    100% { opacity:100%; }
}
@media only screen and (max-width: 1336px) {
    .help-tip p {
        width: 200px;
    }
}
</style>