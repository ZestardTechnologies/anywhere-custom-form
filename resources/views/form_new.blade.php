@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
    ShopifyApp.Bar.initialize({
    title: 'Form',
            buttons: {
            secondary: [{
            label: 'Help',
                    href: '{{ url('help') }}',
                    loading: true
            }, {
            label: 'Global Configuration',
                    href : '{{ url('gobal_configuration') }}',
                    loading: true
            }]
            }
    });
    });</script>

<div id="tabone" class="tab-pane fade in active">
    <ul class="nav nav-tabs" id="CollectionsTab">
        <li class="active" id="form_setting_li"><a data-toggle="tab" href="#collection_settings">Form Settings</a></li>
        <li><a data-toggle="tab" href="#collection_lists" id="field_setting_tab">Fields Settings</a></li>                        
    </ul>
    <div id="wrap">
        <form method="post" class="form-horizontal" action="{{ url('form_create') }}"  onsubmit="return validatemultiform(this);">
            {{ csrf_field() }}
            <div id="multipleforms_wrapper">
                <div class="tab-content">
                    <div id="collection_settings" class="tab-pane mcftab fade in active">
                        <!--<div class="success-copied"></div>-->
                        <div>
                            <input type="hidden" name="form_id" value="<?php echo (isset($form->form_id)) ? $form->form_id : ''; ?>" />
                            <div class="btn-set" style="width: 100%;display: inline-flex;">
                                <h1 class="panel-heading" style="font-size: 24px;width: 100%;"><?php echo 'Form'; ?></h1>
                                <a href="{{ url('dashboard') }}" value="Back" class="btn btn-primary" style="float: right;width: 120px;display: block;vertical-align: middle;height: 40px;margin:0 10px;margin-top: 20px;line-height: 180%;"><span class="glyphicon glyphicon-triangle-left"></span>&nbsp;Back</a>

                                <button type="submit" value="" class="btn btn-primary submitform" style="float: right;width: 170px;display: block;vertical-align: middle;height: 40px;margin-top: 20px;line-height: 180%;" >Save Form &nbsp; <span class="glyphicon glyphicon-floppy-disk"></span></button>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="form_name">Name:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="required-entry form-control" name="form_name" id="form_name" value="<?php echo (isset($form->form_name)) ? $form->form_name : ''; ?>" placeholder="Enter Form Title/Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="form_email">Receive Emails To:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="required-entry form-control" name="form_email" id="form_email" value="<?php echo (isset($form->form_email)) ? $form->form_email : ''; ?>" placeholder="Enter Email" />
                                        <p class="note"><span>Enter comma(,) seperated emails.</a></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="sender">Email Sender:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="required-entry form-control" name="sender" id="sender" value="<?php echo (isset($form->sender)) ? $form->sender : ''; ?>" placeholder="Enter Email Sender" />
                                        <p class="note"><span>Enter sender of the form.</a></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="subject">Subject:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="required-entry form-control" name="subject" id="subject" value="<?php echo (isset($form->subject)) ? $form->subject : ''; ?>" placeholder="Enter Subject" />
                                        <p class="note"><span>Enter subject of the email.</a></span></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php $autoreply = (isset($form->auto_reply_subject)) ? $form->auto_reply : 1; ?>
                                    <label class="control-label col-sm-3" for="auto_reply">Auto Reply:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <select name="auto_reply" class="required-entry form-control" id="auto_reply" onchange="autoreply(this);">
                                            <option <?php echo (isset($form) && $form->auto_reply) ? 'selected' : ''; ?> value="1">Yes</option>
                                            <option <?php echo (isset($form) && !$form->auto_reply) ? 'selected' : ''; ?> value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group autoreplydata" style="<?php echo ($autoreply) ? 'display:block;' : 'display:none;'; ?>">
                                    <label class="control-label col-sm-3" for="auto_reply_sender">Auto Reply Sender:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="form-control" name="auto_reply_sender" id="auto_reply_sender" value="<?php echo (isset($form->auto_reply_sender)) ? $form->sender : ''; ?>" placeholder="Enter Email Sender" />
                                        <p class="note">
                                            <span>Enter auto reply sender.</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group autoreplydata" style="<?php echo ($autoreply) ? 'display:block;' : 'display:none;'; ?>">
                                    <label class="control-label col-sm-3" for="auto_reply_subject">Auto Reply Subject:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <input type="text" class="form-control" name="auto_reply_subject" id="auto_reply_subject" value="<?php echo (isset($form->auto_reply_subject)) ? $form->auto_reply_subject : ''; ?>" placeholder="Enter Auto reply Subject" />
                                        <p class="note">
                                            <span>Enter subject of the auto reply email.</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group autoreplydata" style="<?php echo ($autoreply) ? 'display:block;' : 'display:none;'; ?>">
                                    <label class="control-label col-sm-3" for="auto_reply_message">Auto Reply Message:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <textarea name="auto_reply_message" id="auto_reply_message" placeholder="Enter Auto reply Message"><?php echo (isset($form->auto_reply_message)) ? $form->auto_reply_message : ''; ?></textarea>
                                        @ckeditor('auto_reply_message')
                                        <p class="note">
                                            <span>Enter message of the auto reply email.</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="status">Status:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <select name="status" class="required-entry form-control" id="status">
                                            <option <?php echo (isset($form) && $form->status) ? 'selected' : ''; ?> value="1">Enabled</option>
                                            <option <?php echo (isset($form) && !$form->status) ? 'selected' : ''; ?> value="0">Disabled</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="enable_captcha">Google Captcha:</label>
                                    <div class="col-sm-offset-1 col-sm-7 field">
                                        <select name="enable_captcha" class="required-entry form-control" id="enable_captcha">
                                            <option <?php echo (isset($form) && $form->enable_captcha) ? 'selected' : ''; ?> value="1">Enabled</option>
                                            <option <?php echo (isset($form) && !$form->enable_captcha) ? 'selected' : ''; ?> value="0">Disabled</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if (isset($form->form_encryption_id)) { ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="shortcode">Shortcode:</label>
                                        <div class="col-sm-offset-1 col-sm-7 field">
                                            <div class="success-copied"></div>
                                            <textarea id="ticker-shortcode" rows="1" class="form-control short-code"  readonly="readonly"><?php echo '<div class="Zestard_multi_custom_form" id="' . $form->form_encryption_id . '"><div style=text-align:center;width:100%><img height="130" width="130" src="https://zestardshop.com/shopifyapp/anywhere_custom_forms/public/image/loader_new.svg"></div></div><script src="https://zestardshop.com/shopifyapp/anywhere_custom_forms/public/js/multiformsload.js"></script>'; ?></textarea>
                                            <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;"><i class="fa fa-check"></i>Copy Short Code</button>
                                            <!--                                            <div class="success-copied"></div>-->
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                    </div>    
                    <div id="collection_lists" class="tab-pane mcftab fade">
                        <div class="panel-primary" >
                            <div class="btn-set" style="width: 100%;display: inline-flex;">
                                <h1 class="panel-heading" style="font-size: 24px;width: 100%;"><?php echo 'Form Fields'; ?></h1>
                                <button type="button" value="" class="btn btn-primary add_field" style="float: right;width: 15%;display: block;vertical-align: middle;height: 35px;margin-top: 20px;line-height: 150%;" ><span class="glyphicon glyphicon-plus"></span> &nbsp; Add Field</button>
                            </div>
                            <div class="panel-body fieldsplayground">

                                @if(isset($form->form_id) && $form->form_id != '')
                                @include('optionbox')
                                @endif


                                <input type="hidden" id="deletefieldsids" name="contact[deletefieldsids]" value="">
                            </div>
                            <div class="btn-set" style="width: 100%;display: inline-block;">
                                <button type="submit" value="" class="btn btn-primary submitform" style="float: right;width: 170px;display: block;vertical-align: middle;height: 40px;margin-top: 20px;line-height: 180%;" >Save and Continue &nbsp; <span class="glyphicon glyphicon-floppy-disk"></span></button>
                                <a href="{{ url('dashboard') }}" value="Back" class="btn btn-primary" style="float: right;width: 120px;display: block;vertical-align: middle;height: 40px;margin:0 10px;margin-top: 20px;line-height: 180%;"><span class="glyphicon glyphicon-triangle-left"></span>&nbsp;Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function validatemultiform(data) {
    var required = 0;
    $('.required-entry').css('border-color', '#ccc');
    $('.validation-advice').remove();
    $('.required-entry').each(function() {
    if ($.trim($(this).val()) == '' || $(this).val() == null || $(this).attr("checked") == undefined) {
    if ($.trim($(this).val()) == '' || $(this).val() == null) {
    $(this).css('border-color', '#df280a');
    //alert($(this).attr("id"));
    $(this).closest('table > tbody > tr > td').append('<div class="validation-advice">This is a required field.</div>');
    required += 1;
    //alert("'#"+$(this).attr("id")+"'");
    var currentTab = $(this).closest(".tab-pane").attr("id");
    var activeTab = $('#CollectionsTab li.active a').attr('href');
    if('#'+currentTab != activeTab)
    {
        $('.tab-pane.mcftab').toggleClass('active');
        $('.tab-pane.mcftab').toggleClass('in');
        var activeted = $('#CollectionsTab li.active');
        $('#CollectionsTab li').addClass('active');
        activeted.removeClass('active');
        $('#CollectionsTab li.active a').trigger('click');
    }
        
    //$("'#"+$(this).attr("id")+"'").focus();
    }
    }
    });
    if (required) {
    alert('Please Fill all the required fields for both Form Settings and Fields Settings');    
    return false;
    }
    startloader(1);
    return true;
    }
    function autoreply(data) {
    if (data.value == 0) {
    $('.autoreplydata').hide();
    } else {
    $('.autoreplydata').show();
    }
    }
    $(document).ready(function () {
    $('input[type=radio]').change(function () {
    $('input[type=radio]:checked').not(this).prop('checked', false);
    $(this).parent().prev().prev().find('.select').val('email');
    });
    $('.select').change(function () {
    if ($(this).val() == 'no') {
    $(this).parent().next().next().find('.emailsender').prop('checked', false);
    }
    });
    });
</script>

<!-- <script type="text/javascript">
    function autoreply(data) {
        if (data.value == 0) {
            $('.autoreplydata').hide();
        } else {
            $('.autoreplydata').show();
        }
    }
    function validatemultiform(data) {
        var required = 0;
        $('.required-entry').css('border-color', '#ccc');
        $('.validation-advice').remove();
        $('.required-entry').each(function () {
            if ($(this).val() == '' || $(this).val() == null || $(this).attr("checked") == undefined) {
                if ($(this).hasClass('selection')) {
                    if (!$(this).children().find('.option').is(':checked')) {
                        $(this).css('border-color', '#df280a');
                        $(this).closest('.form-group').children('.col-sm-7').append('<div class="validation-advice">This is a required field.</div>');
                        required += 1;
                    }
                } else if ($(this).val() == '' || $(this).val() == null) {
                    $(this).css('border-color', '#df280a');
                    if ($(this).context.type == 'select-one') {
                        $(this).next('.validation-advice').remove();
                    }
                    $(this).closest('.form-group').children('.field').append('<div class="validation-advice">This is a required field.</div>');
                    required += 1;
                }
            }
        });
        if (required) {
            return false;
        }
        return true;
    }
    $(document).ready(function () {
        $('input[type=radio]').change(function () {
            $('input[type=radio]:checked').not(this).prop('checked', false);
            $(this).parent().prev().prev().find('.select').val('email');
        });

        $('.select').change(function () {
            if ($(this).val() == 'no') {
                $(this).parent().next().next().find('.emailsender').prop('checked', false);
            }
        });
    });
</script> -->
<style>
    #wrap{
        font-size: 12px;
    } 
    .option-box{
        font-size: 12px;
    }
</style>
<style>
    .help-tip{
        position: relative;
        display: inline-block;
        /* top: 18px; 
        right: 18px; */
        left: 5px;
        text-align: center;
        background-color: black;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        font-size: 14px;
        line-height: 15px;
        cursor: pointer;
    }
    
    .help-tip:before{
        content:'i';
        font-weight: bold;
        color:#fff;
    }
    
    .help-tip:hover p{
        display:block;
        transform-origin: 100% 0%;
    
        -webkit-animation: fadeIn 0.3s ease-in-out;
        animation: fadeIn 0.3s ease-in-out;
    
    }
    
    .help-tip p{    /* The tooltip */
        display: none;
        text-align: left;
        background-color: #1E2021;
        padding: 20px;
        width: 300px;
        position: absolute;
        border-radius: 3px;
        box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
        right: auto;
        color: #FFF;
        font-size: 13px;
        line-height: 1.4;
        left: -16px;
        top: 19px;
    }
    
    .help-tip p:before{ /* The pointer of the tooltip */
        position: absolute;
        content: '';
        width:0;
        height: 0;
        border:6px solid transparent;
        border-bottom-color:#1E2021;
        left: 16px;
        top: -12px;
    }
    
    .help-tip p:after{ /* Prevents the tooltip from being hidden */
        width:100%;
        height:40px;
        content:'';
        position: absolute;
        top:-40px;
        left:0;
    }
    
    /* CSS animation */
    
    @-webkit-keyframes fadeIn {
        0% { 
            opacity:0; 
            transform: scale(0.6);
        }
    
        100% {
            opacity:100%;
            transform: scale(1);
        }
    }
    
    @keyframes fadeIn {
        0% { opacity:0; }
        100% { opacity:100%; }
    }
    @media only screen and (max-width: 1336px) {
        .help-tip p {
            width: 200px;
        }
    }
    </style>
@endsection