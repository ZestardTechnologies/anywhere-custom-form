<div class="col-md-12 col-sm-12 col-xs-12">
    <h2 class="dd-help">Setting up the app :</h2> 
    <div class="success-copied"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                            <strong><span class="">How to setup?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>
                        </p>
                    </h4>
                </div>

                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul class="ul-help">
                            <li>Create a new form using <a href = "{{ asset('dashboard') }}" target = "_blank">Create New Form</a> button on dashboard. Add fields as required.</li>
                            <li>Once the form is created, you will get a custom shortcode for that form. Copy the shortcode.   <a  data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/anywhere_custom_forms_001.jpg') }}" href="{{ asset('image/anywhere_custom_forms_001.jpg') }}" target = "_blank">
                                    See Example                               
                                </a>
                            </li>
                            <li>You can create a new page and paste the shortcode in that page to display form in a new page or you can paste it accordingly, where you want to display the form.</li>      
                        </ul>


                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                            <strong><span class="">How to Configure?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>
                        </p>
                    </h4>
                </div>

                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class ="row">
                            <div class ="col-sm-9">
                                <h4><b>Date & Time Options</b></h4>
                                <ol>
                                    <li>
                                        <b>Use JavaScript Calendar : </b>
                                        <br/>
                                        <p> Whether to use javascript calender or not for field type "Date & Time".
                                    </li>

                                    <li>
                                        <b>Date Format: </b>
                                        <br/>
                                        <p> Date format can be changed for field type "Date & Time" using this option.
                                    </li>

                                    <li>
                                        <b>Time Format: </b>
                                        <br/>
                                        <p> 24h or 12h format for field type "Date & Time".
                                    </li>

                                    <li>
                                        <b>Year Range : </b>
                                        <br/>
                                        <p> You can specify year range for field type "Date & Time" using this option.
                                    </li>
                                </ol>
                                <h4><b>Google reCAPTCHA</b></h4>
                                <ul>
                                    <li>
                                        <b>Why Google reCAPTCHA:</b>
                                        <br/>
                                        <p>reCAPTCHA is a free service that protects your site from spam and abuse. It uses advanced risk analysis techniques to tell humans and bots apart.</p>
                                    </li>
                                </ul>
                                
                                <ol>
                                    <li>
                                        <b>Enabled : </b>
                                        <br/>
                                        <p> Google reCAPTCHA is enabled for forms or not.
                                    </li>

                                    <li>
                                        <b>Site Key: </b>
                                        <br/>
                                        <p> Set site key for Google reCAPTCHA.
                                    </li>

                                    <li>
                                        <b>Secret Key : </b>
                                        <br/>
                                        <p> Set Secret key for Google reCAPTCHA.
                                    </li>

                                    <li>
                                        <b>Theme : </b>
                                        <br/>
                                        <p> Change the theme of Google reCAPTCHA.
                                    </li>

                                    <li>
                                        <b>Type: </b>
                                        <br/>
                                        <p> Change the type of Google reCAPTCHA.
                                    </li>

                                    <li>
                                        <b>Size : </b>
                                        <br/>
                                        <p> Change the size of Google reCAPTCHA.
                                    </li>
                                </ol>
                            </div>
                            <div class ="col-sm-3">
                                Add new Form
                                <a  data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/anywhere_custom_forms_001.jpg') }}" href="{{ asset('image/anywhere_custom_forms_001.jpg') }}" target = "_blank">
                                    <img class="img-responsive" src="{{ asset('image/anywhere_custom_forms_001.jpg') }}">                                
                                </a>
                                <br/>
                                Dynamic form fields can be added
                                <a  data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/anywhere_custom_forms_001.jpg') }}" href="{{ asset('image/anywhere_custom_forms_002.jpg') }}" target = "_blank">
                                    <img class="img-responsive" src="{{ asset('image/anywhere_custom_forms_002.jpg') }}">                                
                                </a>
                                <br/>
                                Form displayed in store
                                <a data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/anywhere_custom_forms_001.jpg') }}" href="{{ asset('image/anywhere_custom_forms_003.jpg') }}" target = "_blank">
                                    <img class="img-responsive" src="{{ asset('image/anywhere_custom_forms_003.jpg') }}">                                
                                </a>

                            </div>
                        </div>


                        <!--                        <ul class="ul-help">
                        <li>In case if you don't see the datepicker in your cart page then check for the jquery link in your <a href="<?php echo 'https://' . session('shop') . '/admin/themes/current/?key=layout/theme.liquid' ?>" target="_blank"><b>Theme.liquid</b></a> file.</li>
                        <li>If there is no jquery script then add below short-code in it and then refresh your cart page.</li>
                        <li>Now try to open datepicker again.</li>      
                        </ul>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>