@yield('header')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Anywhere Custom Form</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">   

        <!-- toastr CSS -->
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> 

        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom-style.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
        <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />-->

        <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

        <!-- multi custome form js -->    
        <script src="{{ asset('js/multiforms.js') }}"></script>

        <!-- shopify Script for fast load -->    
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>

        <script type="text/javascript">
ShopifyApp.init({
apiKey: '36c1d04d47a9885dfcd80203a3b11fd4',
        shopOrigin: '<?php echo "https://" . session('shop'); ?>'
});
ShopifyApp.ready(function() {
ShopifyApp.Bar.initialize({
icon: '',
        title: '',
        buttons: {}
});
});
        </script>
        <style>
            .overlay {
                position: fixed;
                display: none;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(0, 0, 0, 0.5);
                z-index: 2;
            }
        </style>
    </head>

    <body style="background-color:#fff">
        <div class="overlay"></div>
        @yield('content')
        <script>
            @if (Session::has('notification'))

                    var type = "{{ Session::get('notification.alert-type', 'info') }}";
            toastr.options = {
            "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
            }
            switch (type){
            case 'info':
                    toastr.info("{{ Session::get('notification.message') }}");
            break;
            case 'warning':
                    toastr.warning("{{ Session::get('notification.message') }}");
            break;
            case 'success':
                    toastr.success("{{ Session::get('notification.message') }}");
            break;
            case 'error':
                    toastr.error("{{ Session::get('notification.message') }}");
            break;
            case 'options':
                    toastr.warning("{{ Session::get('notification.message') }}");
            break;
            }
            @endif
        </script>

        <script type="text/javascript">
                    function copyToClipboard(element) {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($(element).text()).select();
                    document.execCommand("copy");
                    $temp.remove();
                    }
        </script>

        <script>
            jQuery(document).ready(function(){
            jQuery(".copyMe").click(function (){
            var count = jQuery('.show').length;
            if (count == 0){
            jQuery(".show").show();
            jQuery(".success-copied").after('<div class="alert alert-success alert-dismissable show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>');
            }
            });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
            $(".CreateDashboardbtn").on('click', function (event) {
            startloader(1);
            });
            });
            function startloader(process) {
            if (process == 1) {
            $(".overlay").css({
            'display': 'block',
                    'background-image': 'url({{ asset("image/loader.gif") }})',
                    'background-repeat': 'no-repeat',
                    'background-attachment': 'fixed',
                    'background-position': 'center'
            });
            } else {
            $(".overlay").css({
            'display': 'none',
                    'background-image': 'none',
            });
            }
            }
        </script>

        <script type="text/javascript">
            jQuery(function() {
            jQuery('.screenshot').on('click', function() {
            jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
            jQuery('#imagemodal').modal('show');
            });
            });
        </script>

        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>

    </body>
</html>
