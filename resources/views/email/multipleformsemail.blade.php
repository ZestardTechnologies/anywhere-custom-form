<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Zestard Anywheere Custom foarms mail templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div class="container">
<table>
<tbody>
<?php
$config = $fields['config'];
//$this->load->model('Form_Fields_Model');
foreach($fields['fields'] as $key => $value) {
	//$field = $this->Form_Fields_Model->LoadById($key);
        $field = DB::table('zestard_contact_field')->where('field_id', $key)->first();
?>
<tr>
	<td><?php echo $field->title; ?></td>
	<?php
		$Arrayval = '';
		if(is_array($value)) {
			$Arrayval = implode(',', $value);
		}
		if($config['use_jscalendar'] == '0') {
			if($field->type == 'date') {
				if(is_array($value)) {
					$Arrayval = implode('/', $value);
				}
			} else if($field->type == 'time') {
				$Arrayval = $value['hour'].':'.$value['minute'];
			} else if($field->type == 'date_time') {
				$Arrayval = $value['year'].'/'.$value['month'].'/'.$value['day'].' '.$value['hour'].':'.$value['minute'];
				if(isset($value['day_part'])) {
					$Arrayval .= ' '.$value['day_part'];
				}
			}
		}
		if($Arrayval != '') {
			$value = $Arrayval;
		}
	?>
	<td><?php echo ' :   '.$value; ?></td>
</tr>
<?php
}
?>
<tbody>
<table>
</div>
</body>
</html>