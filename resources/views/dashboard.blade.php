@extends('header')
@section('content')
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'All Forms',
            buttons: {
                secondary: [{
                    label: 'Help',
                    href: '{{ url('help') }}',
                    loading: true,                    
                },{
                label: 'Global Configuration',
                href : '{{ url('gobal_configuration') }}',
                loading: true
              }]
            }
        });
    });
</script>

<div class="" >
    <div class="tab-content">

	<div id="tabone" class="tab-pane fade in active">

		<div id="wrap">		

			<div id="multipleforms_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

				<div class="" style="padding-top: 10px">

					<div class="btn-set" style="width: 100%;margin-bottom: 10px;display: inline-flex;">

						<h1 style="font-size: 24px;width: 100%;">Form List</h1>

						<a type="button" href="{{ url('create_new_form') }}" value="Create New Form" class="CreateDashboardbtn btn btn-primary" style="float: right;width: 25%;display: block;vertical-align: middle;height: 40px;margin-top: 20px;line-height: 180%;"><span class="glyphicon glyphicon-plus"></span> Create New Form</a>

					</div>

					<table id="multipleforms" class="table table-striped table-bordered dataTable " cellspacing="0" width="100%" role="grid" aria-describedby="multipleforms_info" >

						<thead>

							  <tr role="row">

								 <th class="sorting_desc" tabindex="0" aria-controls="multipleforms" rowspan="1" colspan="1" aria-label="ID: activate to sort column ascending" aria-sort="descending" style="width: 10px;">ID</th>

								 <th class="sorting" tabindex="1" aria-controls="multipleforms" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending" style="width: 300px;">Title</th>

								 <th class="sorting" tabindex="2" aria-controls="multipleforms" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 40px;">Status</th>

								 <th class="sorting" tabindex="3" aria-controls="multipleforms" rowspan="1" colspan="1" aria-label="Google Captcha: activate to sort column ascending" style="width: 40px;">Google Captcha</th>

								 <th class="sorting" tabindex="4" aria-controls="multipleforms" rowspan="1" colspan="1" aria-label="Email Id: activate to sort column ascending" style="width: 300px;">Email Id</th>

								 <th class="sorting action-class" style="width: 30px;text-align: cente;">Action</th>

							  </tr>

						</thead>			        

						<tbody>
                                                    <?php $form_id = 1; ?>
                                                @foreach($form_records as $form)
							
							<tr role="row" class="">

								<td class="sorting_1" >{{ $form_id }}</td>

								<td>{{ $form->form_name }}</td>

								<td>@if($form->status == 1){{ 'Enabled' }}@else{{ 'Disabled' }}@endif</td>

								<td>@if($form->enable_captcha == 1){{ 'Enabled' }}@else{{ 'Disabled' }}@endif</td>

								<td>{{ $form->form_email }}</td>

								<td>

                                                                    <a href="{{ url('edit_form/'.$form->form_encryption_id) }}" class="CreateDashboardbtn"><span class="glyphicon glyphicon-pencil"></span></a> || <a href="{{ url('form_delete/'.$form->form_encryption_id) }}" class="CreateDashboardbtn" onclick="return confirm('Are you sure want to delete ?')"><span class="glyphicon glyphicon-remove"></span></a>

								</td>

							</tr>
                                                        <?php $form_id++; ?>
						@endforeach	

						</tbody>

					</table>

				</div>		

			</div>

		</div>		

	</div>

	<script type="text/javascript" charset="utf-8">

		$(document).ready(function() {

			$('#multipleforms').DataTable();

		} );

	</script>
</div>
@endsection
