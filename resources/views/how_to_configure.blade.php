<!--<div class="col-md-12 col-sm-12 col-xs-12">
    <h2 class="dd-help">How to Configure?</h2> 
    <div class="success-copied"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                            <strong><span class="">How to Configure?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>
                        </p>
                    </h4>
                </div>

                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class ="row">
                            <div class ="col-sm-6">
                                <h4><b>Date & Time Options</b></h4>
                                <ol>
                                    <li>
                                        <b>Use JavaScript Calendar : </b>
                                        <br/>
                                        <p> Whether to use javascript calender or not for field type "Date & Time".
                                    </li>

                                    <li>
                                        <b>Date Format: </b>
                                        <br/>
                                        <p> Date format can be changed for field type "Date & Time" using this option.
                                    </li>

                                    <li>
                                        <b>Time Format: </b>
                                        <br/>
                                        <p> 24h or 12h format for field type "Date & Time".
                                    </li>

                                    <li>
                                        <b>Year Range : </b>
                                        <br/>
                                        <p> You can specify year range for field type "Date & Time" using this option.
                                    </li>

                                </ol>
                            </div>
                            <div class ="col-sm-6">
                                <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/anywhere_custom_forms_001.jpg') }}" href="javascript:;">
                                    <img class="img-responsive" src="{{ asset('image/anywhere_custom_forms_001.jpg') }}">                                
                                </a>
                            </div>
                        </div>
                        <div class ="row">
                            <h4><b>Google reCAPTCHA</b></h4>
                            <ol>
                                <li>
                                    <b>Enabled : </b>
                                    <br/>
                                    <p> Google reCAPTCHA is enabled for forms or not.
                                </li>

                                <li>
                                    <b>Site Key: </b>
                                    <br/>
                                    <p> Set site key for Google reCAPTCHA.
                                </li>

                                <li>
                                    <b>Secret Key : </b>
                                    <br/>
                                    <p> Set Secret key for Google reCAPTCHA.
                                </li>

                                <li>
                                    <b>Theme : </b>
                                    <br/>
                                    <p> Change the theme of Google reCAPTCHA.
                                </li>

                                <li>
                                    <b>Type: </b>
                                    <br/>
                                    <p> Change the type of Google reCAPTCHA.
                                </li>

                                <li>
                                    <b>Size : </b>
                                    <br/>
                                    <p> Change the size of Google reCAPTCHA.
                                </li>


                            </ol>
                        </div>

                        <ul class="ul-help">
                            <li>In case if you don't see the datepicker in your cart page then check for the jquery link in your <a href="<?php echo 'https://' . session('shop') . '/admin/themes/current/?key=layout/theme.liquid' ?>" target="_blank"><b>Theme.liquid</b></a> file.</li>
                            <li>If there is no jquery script then add below short-code in it and then refresh your cart page.</li>
                            <li>Now try to open datepicker again.</li>      
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                            <strong><span class="">Notes for adding Datepicker into Popup cart</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>
                            In case you want to add datepicker in your  popup cart then copy and paste the following code into Your popup cart page.

                        </p>                                    
                        <div class="copystyle_wrapper col-md-5">
                            <textarea rows="1" class="form-control popup_code" id="popup_code" disabled><?php echo "{% include 'delivery-date' %}" ?></textarea>
                            <btn value="Copy Shortcode" class="btn btn-info copycss_button tooltipped tooltipped-s copyMe" style="display: block;" onclick="copyToClipboard('#popup_code')"><i class="fa fa-check"></i> Copy</btn>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->