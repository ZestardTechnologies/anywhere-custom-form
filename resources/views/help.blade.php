@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
                secondary: [{
                        label: 'Global Configuration',
                        href: '{{ url('gobal_configuration') }}',
                        loading: true
                    }]
            }
        });
    });
</script>

<?php
$store_name = session('shop');
?>

<style>
    .modal-dialog img {
        width: 100%;
        text-align: center;
    }
</style>

<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="help_page">
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>

                    <div class ="row">
                        <div class =col-md-12 col-sm-12 col-xs-12">
                                <h2 class="dd-help">Please refer the below screencast if you need help configuring the app.</h2>
                                <center>
                                    <video controls style="width: 100%; height : 100%">
                                       <source src="{{ asset('screencast/final_screencast_anywhere_custom_form.mp4') }}" type="video/mp4">
                                        Your browser does not support the HTML 5 video tag. Please try opening in another Browser.
                                    </video>
                                </center>
                        </div>
                    </div>
                </div>
            </div>

            @include('how_to_setup')

            @include('how_to_configure')
            
            @include('how_to_uninstall')

        </div>

        <!--Version updates-->
        <!--
                <div class="version_update_section">
        
                    <div class="col-md-6" style="padding-right: 0;">
        
                        <div class="feature_box">
        
                            <h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
        
                            <div class="version_block">
        
                                <div class="col-md-12">
        
                                    <div class="col-md-3 version_date">
        
                                        <p><i class="glyphicon glyphicon-heart"></i></p>
        
                                        <strong>22 Jan, 2018</strong>
        
                                        <a href="#"><b>Update</b></a>
        
                                    </div>
        
                                    <div class="col-md-8 version_details">
        
                                        <strong>Version 2.0</strong>
        
                                        <ul>
        
                                            <li>Add Delivery Date & Time information under customer order Email</li>
        
                                            <li>Auto Select for Next Available Delivery Date</li>
        
                                            <li>Auto Tag Delivery Details to all the Orders</li>
        
                                            <li>Manage Cut Off Time for Each Individual Weekday</li>
        
                                            <li>Limit Number of Order Delivery during the given time slot for any day </li>
        
                                        </ul>
        
                                    </div>
        
                                </div>
        
                                <div class="col-md-12">
        
                                    <div class="col-md-3 version_date">
        
                                        <p><i class="glyphicon glyphicon-globe"></i></p>
        
                                        <strong>20 Dec, 2017</strong>
        
                                        <a href="#"><b>Release</b></a>
        
                                    </div>
        
                                    <div class="col-md-8 version_details version_details_2">
        
                                        <strong>Version 1.0</strong>
        
                                        <ul>
        
                                            <li>Delivery Date & Time Selection</li>
        
                                            <li>Same Day & Next Day Delivery</li>
        
                                            <li>Blocking Specific Days and Dates</li>
        
                                            <li>Admin Order Manage & Export Based on Delivery Date</li>
        
                                            <li>Option for Cut Off Time & Delivery Time</li>
        
                                        </ul>
        
                                    </div>
        
                                </div>
        
                            </div>
        
                        </div>
        
                    </div>
        
                    <div class="col-md-6">
        
                        <div class="feature_box">
        
                            <h3 class="dd-help">Upcoming Features</h3>
        
                            <div class="feature_block">
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
        
                                        <label for="checkbox0"></label>
        
                                    </span> 
        
                                    <strong>Multiple Cutoff Time Option</strong>  
        
                                </div>
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
        
                                        <label for="checkbox4"></label>
        
                                    </span> 
        
                                    <strong>Multiple Delivery Time Option</strong>  
        
                                </div>
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
        
                                        <label for="checkbox5"></label>
        
                                    </span> 
        
                                    <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
        
                                </div>
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
        
                                        <label for="checkbox1"></label>
        
                                    </span> 
        
                                    <strong>Auto Select for Next Available Delivery Date</strong>  
        
                                </div>
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
        
                                        <label for="checkbox2"></label>
        
                                    </span> 
        
                                    <strong>Order Export in Excel</strong>  
        
                                </div>
        
                                <div>   
        
                                    <span class="checkboxFive">
        
                                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
        
                                        <label for="checkbox3"></label>
        
                                    </span> 
        
                                    <strong>Filtering Orders by Delivery Date</strong>  
        
                                </div>                                        
        
                            </div>
        
                            <div>
        
                                <p class="feature_text">
        
                                    New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
        
                                </p>
        
                            </div>
        
                        </div>
        
                    </div>
        
                </div>-->
    </div>
</div>

<script>

    $(document).ready(function () {

        // Add minus icon for collapse element which is open by default

        $(".collapse.in").each(function () {

            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");

        });



        // Toggle plus minus icon on show hide of collapse element

        $(".collapse").on('show.bs.collapse', function () {

            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");

        }).on('hide.bs.collapse', function () {

            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");

        });

        $(".screenshot").click(function () {
            $(".modal-content img").attr("src", $(this).attr("data-src"));
            $(".modal-title").html($(this).data("title"));
        });

    });
</script>
@endsection