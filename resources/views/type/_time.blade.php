<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker-master/jquery.datetimepicker.css') }}"/>

<label  class="anywhere-custom-label" for="<?php echo $row->type . $row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">' . $row->title . '<em class="text-danger">*</em>' : '>' . $row->title; ?></label>
<div class="anywhere-custom-control-box requireddatetime field">
    <?php $is_required = ($row->is_require) ? 'required' : ''; ?>
    <?php
    $class = $row->type . $row->field_id;
    $ConfigFormat = explode('-', $config->year_range);
    ?>
    <?php if ($config->use_jscalendar) { ?>
        <div class="input-group date <?php echo $class; ?> form_time anywhere-custom-control" data-date-format="HH:ii P" data-link-field="dtp_input3" data-link-format="HH:ii P">
            <input class="<?php echo ($row->is_require) ? 'required-entry' : ''; ?> anywhere-custom-control" size="16" type="text" name="<?php echo 'fields[' . $row->field_id . ']'; ?>" >
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
        </div>
        <!--<script type="text/javascript">
        <?php if ($config->time_format == '1') { ?>
            $('.<?php echo $class; ?>').datetimepicker({
                    weekStart: 0,
                    todayBtn:  0,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 1,
                    minView: 0,
                    showMeridian: true,
                    maxView: 1,
                    forceParse: 1
            });
        <?php } else { ?>
            $('.<?php echo $class; ?>').datetimepicker({
                    language:  'us',
                    weekStart: 0,
                    todayBtn:  0,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 1,
                    minView: 0,
                    showMeridian: true,
                    maxView: 1,
                    forceParse: 1
            });
        <?php } ?>
        </script>-->

        <script type ="text/javascript">
            jQuery('.<?php echo $class; ?> input').datetimepicker({
                datepicker: false,
                format: 'H:i'
            });
        </script>


    <?php
    } else {
        $mydata['row'] = $row;
        $mydata['count'] = $count;
        $mydata['is_required'] = $row->is_require;
        ?>
    <!-- <span class="time-picker"> -->
        <?php if ($config->time_format == 1): ?>
            <?php $mydata['format'] = 1; ?>
            <?php $this->load->view('type\format\hour', $mydata); ?>
        <?php $this->load->view('type\format\minute', $mydata); ?>
            &nbsp;
            <select name="<?php echo 'fields[' . $row->field_id . '][day_part]'; ?>" id="<?php echo 'fields_' . $row->field_id . '_day_part'; ?> "  title="Day Part">
                <option value="am" selected="selected">AM</option>
                <option value="pm">PM</option>
            </select>
        <?php else: ?>
            <?php $mydata['format'] = 2; ?>
            <?php $this->load->view('type\format\hour', $mydata); ?>
            <?php $this->load->view('type\format\minute', $mydata); ?>
        <?php endif; ?>
        <!-- </span> -->
<?php } ?>
</div>
