<?php 
$form = (object) $form['form'];
//dd((object) $form['form']);
if ($form->enable_captcha): ?>
<div class="col-sm-offset-5 col-sm-7">
<?php $id = uniqid(); ?>
<div class="recaptcha" style="overflow:hidden;position:relative;margin-bottom:10px;">
	<input type="checkbox" id="cb-<?php echo $id ?>" name="g-recaptcha-response" value="" class="hide required-entry" style="visibility:hidden; position:absolute; left:-1000000px" />
	<div id="el-<?php echo $id ?>"></div>
	<script type="text/javascript">
		var onloadCallback = function() {
			grecaptcha.render('el-<?php echo $id ?>', {
				'sitekey': "<?php echo $config->site_key; ?>",
				'theme': "<?php echo $config->theme; ?>",
				'type': "<?php echo ($config->type == 1) ? 'image':'audio'; ?>",
				'size': "<?php echo ($config->size == 1) ? 'normal':'compact'; ?>",
				'callback': function(response) {
					if (response.length > 0) {
						console.log(response.length);
						$('#cb-<?php echo $id ?>').val(1);
						$('#cb-<?php echo $id ?>').checked = true;
					}
				}
			});
		};
	</script>
	<?php
		$lang = 'en';
		$query = array(
			'onload' => 'onloadCallback',
			'render' => 'explicit',
			'hl'     => $lang
		);
		echo sprintf('<script src="https://www.google.com/recaptcha/api.js?%s" async defer></script>',http_build_query($query));
	?>
</div>
</div>
<?php endif; ?>
