<label  class="anywhere-custom-label" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger">*</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box field">
<input type="file" name="<?php echo 'fields'.$row->field_id; ?>" class="<?php echo ($row->is_require) ? 'required-entry':''; ?>" id="<?php echo $row->type.$row->field_id ?>">
<p class="note"><span>Allowed file types <b><?php echo $row->file_extension; ?></b></span></p>
<p class="note"><span>Max File Size <b><?php echo $row->file_size ; ?>MB</b></span></p>
</div>
