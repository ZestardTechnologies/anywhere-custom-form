<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker-master/jquery.datetimepicker.css') }}"/>

<label  class="anywhere-custom-label" for="<?php echo $row->type . $row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">' . $row->title . 
        '<em class="text-danger">*</em>' : '>' . $row->title; ?></label>
<div class="anywhere-custom-control-box requireddatetime field">
    <?php
    $class = $row->type . $row->field_id;
    $ConfigFormat = explode('-', $config->year_range);
    $format = unserialize($config->date_field_order);
    $DateFormat = '';
    $cnt = 0;
    $CollectView = array();
    foreach ($format as $op) {
        $DateFormat .= ($cnt) ? '/' : '';
        if ($op != 'y') {
            $DateFormat .= $CollectView[] = $op . $op;
        } else {
            $DateFormat .= $CollectView[] = $op . $op . $op . $op;
        }
        $cnt++;
    }
    if ($config->use_jscalendar) {
        ?>
        <div class="input-group date <?php echo $class; ?> anywhere-custom-control" data-date="" data-date-format="<?php echo $DateFormat . ' HH:ii P'; ?>" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii P">
            <input class="<?php echo ($row->is_require) ? 'required-entry' : ''; ?> anywhere-custom-control" size="16" type="text" name="<?php echo 'fields[' . $row->field_id . ']'; ?>" id="<?php echo $row->type . $row->field_id ?>" title="<?php echo $row->title ?>" >
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    <script type ="text/javascript">
       $('#<?php echo $class; ?>').datetimepicker();
    </script>
    <?php
    } else {
        $mydata['row'] = $row;
        $mydata['count'] = $count;
        $mydata['is_required'] = $row->is_require;
        $mydata['Yearrange'] = range($ConfigFormat[0], $ConfigFormat[1]);
        foreach ($CollectView as $view) {
            $this->load->view('type\format\\' . $view, $mydata);
        }
        ?>
        <?php if ($config->time_format == 1): ?>
            <?php $mydata['format'] = 1; ?>
            <?php $this->load->view('type\format\hour', $mydata); ?>
        <?php $this->load->view('type\format\minute', $mydata); ?>
            &nbsp;
            <select name="<?php echo 'fields[' . $row->field_id . '][day_part]'; ?>" id="<?php echo 'fields_' . $row->field_id . '_day_part'; ?> "  title="Day Part">
                <option value="am" selected="selected">AM</option>
                <option value="pm">PM</option>
            </select>
        <?php else: ?>
            <?php $mydata['format'] = 2; ?>
            <?php $this->load->view('type\format\hour', $mydata); ?>
            <?php $this->load->view('type\format\minute', $mydata); ?>
        <?php endif; ?>
<?php } ?>
    <span>&nbsp;&nbsp;&nbsp;<?php echo $DateFormat; ?></span>
</div>