<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        	<div class="navbar navbar-default navbar-static-top" style="margin:10px auto;background: #EBEEF0;">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Navigation</span>
					<span class="fa fa-bar"></span>
					<span class="fa fa-bar"></span>
					<span class="fa fa-bar"></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav" id="navbars">
                                    <li class="<?php echo ($page == 'product_settings' ? 'active' : '') ?>">
						<a href="<?php echo base_url('Products/product_settings'); ?>">
							<i class="fa fa-info-circle"></i>&nbsp;Product-Settings
						</a>
					</li>
                                    <li class="<?php echo ($page == 'product_List' ? 'active' : '') ?>">
						<a href="<?php echo base_url('Products/'); ?>">
							<i class="fa fa-info-circle"></i>&nbsp;Product List
						</a>
					</li>
                                        <li class="<?php echo ($page == 'global_config_option' ? 'active' : '') ?>">
						<a href="<?php echo base_url('Products/saveconfig'); ?>">
							<i class="fa fa-info-circle"></i>&nbsp;Global Configuration
						</a>
					</li>
					<li class="<?php echo ($page == 'help' ? 'active' : '') ?>">
						<a href="<?php echo base_url('help/'); ?>">
							<i class="fa fa-info-circle"></i>&nbsp;Help
						</a>
					</li>

				</ul>
			</div>
		</div>
	</div>
    </div>
</div>
        

