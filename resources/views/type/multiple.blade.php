<label  class="anywhere-custom-label" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger"> *</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box field">
<select name="<?php echo 'fields['.$row->field_id.'][]'; ?>" class="<?php echo ($row->is_require) ? 'required-entry':''; ?> anywhere-custom-control" id="<?php echo $row->type.$row->field_id ?>" title="<?php echo $row->title; ?>" multiple="multiple">
<?php
$childoptions = unserialize($row->child_options);
$cnt = 1;
$is_required = ($row->is_require)? 'required' : '';
foreach($childoptions as $option){
	echo '<option value="'.$option['title'].'">'.$option['title'].'</option>';
	$cnt++;
}
?>
</select>
</div>