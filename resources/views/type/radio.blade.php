<label class="anywhere-custom-label col-sm-4"  <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger"> *</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box field">
<?php
$childoptions = unserialize($row->child_options);
$cnt = 1;
$is_required = ($row->is_require)? 'required-entry' : '';
?>
<div class="<?php echo $is_required; ?> selection">
<?php
foreach($childoptions as $option){
	echo '<label class="radio-inline"><input type="radio" class="option" name="fields['.$row->field_id.']" id="fields_'.$row->field_id.'_'.$cnt.'" value="'.$row->title.'">'.$option['title'].'</label>';
	$cnt++;
}
?>
</div>
</div>