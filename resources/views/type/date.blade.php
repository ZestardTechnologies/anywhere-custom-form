<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker-master/jquery.datetimepicker.css') }}"/>
<?php //dd($row->type.$row->field_id); ?>
<label  class="anywhere-custom-label " for="<?php echo $row->type.$row->field_id; ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger">*</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box requireddatetime field">
<?php
$class = $row->type.$row->field_id;
$ConfigFormat = explode('-',$config->year_range);
$format = unserialize($config->date_field_order);
$DateFormat = '';
$cnt = 0;
$CollectView = array();
$count = $row->count;
foreach($format as $op) {
	$DateFormat .= ($cnt) ? '/' : '';
	if($op != 'y') {
		$DateFormat .= $CollectView[] = $op;
	} else {
		$DateFormat .= $CollectView[] =strtoupper($op);
	}
	$cnt++;
}
if($config->use_jscalendar) {
?>
<div class="input-group date <?php echo $class; ?> anywhere-custom-control" data-date="" data-date-format="<?php echo $DateFormat; ?>" data-link-field="dtp_input2" data-link-format="Y-m-d">
	<input class="<?php echo ($row->is_require) ? 'required-entry':''; ?> anywhere-custom-control" size="16" type="text" name="<?php echo 'fields['.$row->field_id.']'; ?>" id="<?php echo $row->type.$row->field_id ?>" title="<?php echo $row->title ?>" >
	<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>


<script type ="text/javascript">
    zestard_acf('#<?php echo $row->type.$row->field_id ?>').datetimepicker({
	format: '<?php echo $DateFormat; ?>',
        yearStart: '<?php echo $ConfigFormat[0]; ?>',
        yearEnd: '<?php echo $ConfigFormat[1]; ?>',
        timepicker: false,
    });
</script>

<?php } else {
$mydata['row'] = $row;
$mydata['count'] = $count;
$mydata['Yearrange'] = range($ConfigFormat[0],$ConfigFormat[1]);
foreach($CollectView as $view) {
        $view_check = 'type.format..'.$view;
        $mydata_row = (object) $mydata;
        if (View::exists($view_check)) {
        echo view($view_check)->with(['row' => $mydata_row])->render();
    }
    //$this->load->view('type\format\\'.$view , $mydata);   
}
?>
<?php } ?>
<span>&nbsp;&nbsp;&nbsp;<?php echo $DateFormat; ?></span>
</div>
