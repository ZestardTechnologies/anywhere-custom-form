<label  class="anywhere-custom-label" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger">*</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box requireddatetime field">
<?php $is_required = ($row->is_require)? 'required' : ''; ?>
<?php
$class = $row->type.$row->field_id;
$ConfigFormat = explode('-',$config->year_range);
?>
<?php if($config->use_jscalendar) { ?>
<div class="input-group date <?php echo $class; ?> form_time anywhere-custom-control" data-date-format="H:i" data-link-field="dtp_input3" data-link-format="h:i">
	<input class="<?php echo ($row->is_require) ? 'required-entry':''; ?> anywhere-custom-control" size="16" type="text" name="<?php echo 'fields['.$row->field_id.']'; ?>" id="<?php echo $row->type.$row->field_id ?>" >
	<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
</div>
<script type="text/javascript">
<?php if($config->time_format == '1') { ?>
zestard_acf('#<?php echo $row->type.$row->field_id ?>').datetimepicker({
	datepicker: false,
        defaultDate: false,
        formatTime: 'H:i',
        format: 'H:i',
        formatDate: false,
        hours12: false
});
<?php } else { ?>
zestard_acf('#<?php echo $row->type.$row->field_id ?>').datetimepicker({
	datepicker: false,
        defaultDate: false,
        formatTime: 'H:i',
        format: 'H:i',
        formatDate: false,
        hours12: false
});
<?php } ?>
</script>
<?php } else {
$mydata['row'] = $row;
$mydata['count'] = $count;
$mydata['is_required'] = $row->is_require;
?>
<!-- <span class="time-picker"> -->
<?php if($config->time_format == 1): ?>
	<?php $mydata['format'] = 1; ?>
	<?php $this->load->view('type\format\hour', $mydata); ?>
   <?php $this->load->view('type\format\minute', $mydata); ?>
   &nbsp;
   <select name="<?php echo 'fields['.$row->field_id.'][day_part]'; ?>" id="<?php echo 'fields_'.$row->field_id.'_day_part'; ?> "  title="Day Part">
      <option value="am" selected="selected">AM</option>
      <option value="pm">PM</option>
   </select>
<?php else: ?>
	<?php $mydata['format'] = 2; ?>
	<?php $this->load->view('type\format\hour', $mydata); ?>
	<?php $this->load->view('type\format\minute', $mydata); ?>
<?php endif; ?>
<!-- </span> -->
<?php } ?>
</div>
