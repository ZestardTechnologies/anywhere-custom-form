<label  class="anywhere-custom-label" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger"> *</em>': '>'.$row->title; ?></label>
<div class="anywhere-custom-control-box field">
<?php
$childoptions = unserialize($row->child_options);
$cnt = 1;
$is_required = ($row->is_require)? 'required-entry' : '';
?>
<div class="<?php echo $is_required; ?> selection">
<?php
foreach($childoptions as $option){
	echo '<label class="checkbox-inline"><input type="checkbox" name="fields['.$row->field_id.'][]" class="option" id="fields_'.$row->field_id.'_'.$cnt.'" value="'.$option['title'].'">'.$option['title'].'</label>';
	$cnt++;
}
?>
</div>
</div>