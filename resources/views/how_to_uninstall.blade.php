<div class="col-md-12 col-sm-12 col-xs-12">
    <h2 class="dd-help">How to uninstall?</h2> 
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion3" href="#collapse11"> 
                            <strong><span class="">Uninstall instructions</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse11" class="panel-collapse collapse">       
                    <div class = "panel-body">
                        <div class = "product_matrix_screenshot_box">
                            <div class = "container">
                                <div class ="row">
                                    <div class ="col-sm-5">
                                        <ul class="ul-help">
                                            <li>To Remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                                            <li>Click on the delete icon of Anywhere Custom Form App.</li>
                                            <li>If Possible then Remove Shortcode where you have Pasted.</li>
                                        </ul>
                                    </div>
                                    <div class ="col-sm-6">
                                        Check this screenshot for uninstalling the app.
                                        <br/>
                                        <a data-toggle="modal" class="info_css screenshot" data-title="Uninstall Anywhere Custom Form"  href="{{ asset('image/uninstall.png') }}" target = "_blank">
                                            <img class ="img-responsive" src ="{{ asset('image/uninstall.png') }}" style = "max-width : 70%;"/>                                
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>