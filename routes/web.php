<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::any('get_form_data', 'FrontendController@frontend')->middleware('cors')->name('get_form_data');

Route::any('send_email', 'SendmailController@sendMail')->middleware('cors')->name('send_email');

Route::get('declined', 'callbackController@declined')->name('declined');

Route::any('form_create', 'FormController@store')->name('form_create');

Route::any('edit_form/{id}', 'FormController@edit')->name('edit_form');

Route::any('form_delete/{id}', 'FormController@delete')->name('form_delete');
Route::any('gobal_configuration', 'GlobalconfigController@index')->name('gobal_configuration');
Route::any('gobal_configuration_save', 'GlobalconfigController@store')->name('gobal_configuration_save');
Route::get('decline', function () {
    return view('decline');
})->name('decline');

Route::get('create_new_form', function () {
    return view('form_new');
})->name('create_new_form');

Route::get('help', function () {
    return view('help');
})->name('help');

Route::any('anywhere_custome_form_email_chron', 'MailCroneController@initiatecron')->name('anywhere_custome_form_email_chron');

Route::any('after_mail', 'SendmailController@aftermail')->middleware('cors')->name('after_mail');