<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ZestardContactForm extends Model
{
  protected $table = 'zestard_contact_form';
  protected $primaryKey = 'form_id';
  public $timestamps = false;
  protected $fillable =[
    'form_encryption_id',
    'form_name',
    'status',
    'subject',
    'form_email',
    'sender',
    'auto_reply',
    'auto_reply_subject',
    'auto_reply_sender',
    'auto_reply_message',
    'enable_captcha',
    'store_id'
  ];
}
