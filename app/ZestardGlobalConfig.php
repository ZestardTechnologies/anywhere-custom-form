<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ZestardGlobalConfig extends Model
{
  protected $table = 'zestard_global_config';
  protected $primaryKey = 'config_id';
  public $timestamps = false;
  protected $fillable =[
    'store_id',
    'use_jscalendar',
    'date_field_order',
    'time_format',
    'year_range',
    'enable_captcha',
    'site_key',
    'secret_key',
    'theme',
    'type',
    'size'
  ];
}
