<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ZestardContactField extends Model
{
  protected $table = 'zestard_contact_field';
  protected $primaryKey = 'field_id';
  public $timestamps = false;
  protected $fillable =[
    'form_id',
    'title',
    'type',
    'is_require',
    'max_characters',
    'validation',
    'email_sender',
    'file_extension',
    'file_size',
    'sort_order',
    'child_options',
    'store_id'
  ];
}
