<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSend extends Model
{
  protected $table = 'email_records';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable =[
    'email_template',
    'sender',
    'receiver',
    'subject',
    'attachment_path',
    'store_id',
    'email_status'
  ];
}
