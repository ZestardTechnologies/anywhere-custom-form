<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use App\AddNewsForm;
use App;
use DB;
use App\ShopModel;
use App\ZestardContactForm;
use App\ZestardContactField;
use App\ZestardGlobalConfig;

class FrontendController extends Controller
{
    public function frontend(request $request)
    {
            $shop_find = ShopModel::where('store_name' , $request['shop'])->first();
            $data_array = [];
            session(['shop' => $request['shop']]);
            
            if($shop_find->status == "active")
            {       
                foreach ($request['id'] as $encrypt_id){
                    $form_find = ZestardContactForm::where('form_encryption_id' , $encrypt_id)->first()->toArray();
                    $field_find = ZestardContactField::where('form_id' , $form_find['form_id'])->orderBy('sort_order')->get()->toArray();
                    $config = ZestardGlobalConfig::where('store_id' , $shop_find->id)->first();
                    //dd($config);
                    $view = view('frontview',['form'=>$form_find,'fields'=>$field_find,'config'=>$config])->render();
                    $content = (string)$view;
                    $form_find['html'] = $content;
                    array_push($data_array,$form_find);    
                    //$data_array['html'] = $content;
                }                                
                return json_encode($data_array);
            }
            else{
                return json_encode($data_array);
            }
    }
}
