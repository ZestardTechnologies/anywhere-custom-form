<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;
use App\ShopModel;
use App\ZestardContactForm;
use App\ZestardContactField;

class FormController extends Controller {

    public function store(request $request) {
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name', $shop_name)->first();

        if ($request->isMethod('post')) {
            // If route method is POST
            if ($request->form_id > 0) {
                //for updating the form record
                //$request['store_id'] = $shop_find->id;
                $form_find = ZestardContactForm::where('form_id', $request->form_id)->first();
                $form_find->update($request->all());

                //delete the removed fields from the database
                if (isset($request['contact']['deletefieldsids']) && $request['contact']['deletefieldsids'] != '') {
                    $delete_id = $request['contact']['deletefieldsids'];
                    $delete_id = explode(",", $delete_id);
                    $delete_form = ZestardContactField::whereIn('field_id', $delete_id)->delete();
                }

                //updating the field records
                if (isset($request['contact']['fields'])) {
                    $field_data = $request['contact']['fields'];
                    foreach ($field_data as $fields) {
                        $fields['email_sender'] = ((isset($fields['email_sender']) && $fields['email_sender'] == 'on')) ? 1 : 0;
                        if (isset($fields['values'])) {
                            $fields['child_options'] = serialize($fields['values']);
                        }

                        //for updating the existing fields
                        if (isset($fields['field_id'])) {
                            $field_find = ZestardContactField::where('field_id', $fields['field_id'])->first();
                            $fields['store_id'] = $shop_find->id;
                            $field_find->update($fields);
                        } else {//for creating the new field if user added any new fields
                            $fields['form_id'] = $request->form_id;
                            $fields['store_id'] = $shop_find->id;
                            $new_added_field = ZestardContactField::create($fields);
                        }
                    }
                }
                $notification = array(
                    'message' => 'Form updated Succesfully.',
                    'alert-type' => 'success'
                );
                return redirect()->back()->with('notification', $notification);
            } else {
                $request['store_id'] = $shop_find->id;
                $new_form = ZestardContactForm::create($request->all());
                $last_form_id = $new_form->form_id . $shop_find->id;
                $form_encrypt = crypt($last_form_id, "ze");
                $final_form_encrypt = str_replace(['/', '.'], "Z", $form_encrypt);
                $update_form__encryption = ZestardContactForm::where('form_id', $new_form->form_id)->update(['form_encryption_id' => $final_form_encrypt]);
                if (isset($request['contact']['fields'])) {

                    $field_data = $request['contact']['fields'];

                    foreach ($field_data as $fields) {
                        $fields['form_id'] = $new_form->form_id;
                        $fields['store_id'] = $shop_find->id;
                        $fields['email_sender'] = ((isset($fields['email_sender']) && $fields['email_sender'] == 'on')) ? 1 : 0;
                        if (isset($fields['values'])) {
                            $fields['child_options'] = serialize($fields['values']);
                        }
                        $form_fields = ZestardContactField::create($fields);
                    }
                }

                $notification = array(
                    'message' => 'form Saved Succesfully.',
                    'alert-type' => 'success'
                );
                
                 return redirect('dashboard')->with('notification', $notification);
            }
        } else {
            // If route method is GET
            $inserted_form = ZestardContactForm::where('form_id',$new_form->form_id)->first();
            $inserted_form_id = $inserted_form->form_id;
            $inserted_form_Fields = ZestardContactField::where('form_id',$inserted_form_id)->get();
            return view('form_new',['notification' => $notification,'form' =>$inserted_form,'Fields' =>$inserted_form_Fields]);    
        }
    }

    public function edit(request $request, $id) {
        $form = ZestardContactForm::where('form_encryption_id', $id)->first();
        $form_id = $form->form_id;
        $Fields = ZestardContactField::where('form_id', $form_id)->get();
        return view('form_new', ['form' => $form, 'Fields' => $Fields]);
    }

    public function delete(request $request, $id) {
        $delete_form = ZestardContactForm::where('form_encryption_id', $id)->delete();
        $notification = array(
            'message' => 'form Deleted Succesfully.',
            'alert-type' => 'success'
        );
        return redirect('dashboard')->with('notification', $notification);
    }

}
