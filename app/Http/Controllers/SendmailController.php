<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use App\AddNewsForm;
use App;
use DB;
use App\ShopModel;
use App\ZestardContactForm;
use App\ZestardContactField;
use App\ZestardGlobalConfig;
use App\EmailSend;
use Mail;

class SendmailController extends Controller
{
    public function sendMail(request $request)
    {
        $data = $request->all();
        $body = '';
        $attachment = array();
        $names = array();
        $temp_array= array();
        $sender = '';
        $receiver = '';
        $subject = '';
        $store_id = '';
        
        try {
            if (isset($data['form_id'])) {
                $form = ZestardContactForm::where('form_id' , $data['form_id'])->first();
                $store_id = $form->store_id;
            } else {
                //$this->session->set_flashdata('type', 'error');
                //$this->session->set_flashdata('message', 'Something went wrong !!!');
                //redirect('/contact/');
            }
            $row = ZestardContactField::where('form_id' , $data['form_id'])->where('email_sender', 1)->first();
            
            if($row != ''){
                $sender = $data['fields'][$row->field_id];
            } else {
                $sender = $form->sender;
            }
            //dd('test');
            //$data['config'] = ZestardGlobalConfig::where('store_id' , $row->store_id)->first();
            $data['config'] = ZestardGlobalConfig::where('store_id' , $store_id)->first();
           //dd('test');
            $data['sender'] = $form->sender;
            $SenderField = ZestardContactField::where('form_id' , $data['form_id'])->where('email_sender', 1)->first();
            $data['sender'] = ZestardContactField::where('form_id' , $data['form_id'])->where('email_sender', 1)->first();
            if (count($SenderField) > 0) {
                $data['sender'] = $data['fields'][$SenderField->field_id];
            }
            $receiver = $form->form_email;
            $subject = $form->form_name;
            $body = view('email.multipleformsemail',['fields' => $data]);
            $warningCount = 0;
            $errors = '';
            $aConfig = array();
            foreach ($_FILES as $key => $file) {
                $FieldId = str_replace('fields', '', $key);
                $field = ZestardContactField::where('field_id', $FieldId)->first();
                $AllowedExt = explode(',', $field->file_extension);
                $AllowedExt = implode('|', $AllowedExt);
                $fileSize = $field->file_size * 1024;
                $warningCount = 0;                
                if ($_FILES[$key]['size'] > 0) {
                    $aConfig['upload_path'] = public_path('mailed/'.$FieldId);
                    $aConfig['allowed_types'] = $AllowedExt;
                    $aConfig['max_size'] = $fileSize;
                    if (!is_dir($aConfig['upload_path'])) {
                        mkdir($aConfig['upload_path'], 0777, true);
                    }
                    $aConfig['upload_path'] = $aConfig['upload_path'].'/'.$_FILES[$key]['name'];
                    
                    if (move_uploaded_file($_FILES[$key]['tmp_name'], $aConfig['upload_path'])) {
                        $name = $_FILES[$key]['name'];
                        $path = $aConfig['upload_path'];                        
                        $temp_array[$name] = $path ;
                        //array_push($attachment,$temp_array);
                        //$attachment = $aConfig['upload_path'];
                        //$this->email->attach($pathToUploadedFile);
                    } else {           
                        $warningCount++;
                        //$errors .= $this->upload->display_errors();
                    }
                }
               
            }
            
            $attachment_insert = json_encode($temp_array);
            $find_shop = ShopModel::where('id' , $store_id)->first();
            $shop = $find_shop->store_name;
            $email_record_insert = EmailSend::create(['email_template' => $body, 'sender' => $sender, 'receiver' => $receiver, 'subject' => $subject, 'attachment_path' => $attachment_insert, 'store_id' => $store_id]);
            session(['shop' => $shop]);
            //Send mail without cron start
            $email_records = EmailSend::where('email_status' , 0)->limit(10)->get();
        
                if(count($email_records) > 0){
                    foreach ($email_records as $email_data)
                    {
                        $body = $email_data->email_template;
                        $sender = $email_data->sender;
                        $receiver = $email_data->receiver;
                        $subject = $email_data->subject;
                        $attachment = $email_data->attachment_path;
                        $store_id = $email_data->store_id;
                        $shop_find = ShopModel::where('id' , $store_id)->first();
                        $shop = $shop_find->store_name;
                        $temp_array = (array)json_decode($attachment);
                        $id = $email_data->id;
                        $multiple_receiver = explode(",",$receiver);
                        try {
                            Mail::raw([], function ($message) use($temp_array,$sender,$multiple_receiver,$subject,$shop,$body) {
                                $message->from($sender,$shop);
                                $message->to($multiple_receiver)->subject($subject);
                                foreach($temp_array as $key => $value){
                                    $message->attach($value, [
                                            'as' => $key ]);                    
                                }
                                $message->setBody($body, 'text/html');
                            });

                            EmailSend::where('id' , $id)->update(['email_status' => 1]);
                            //Log::info('Mail Cron sended mail for the record id '.$id);

                        }catch (\Exception $ex) {
                            // Debug via $ex->getMessage();
                            //Log::info('Mail Cron failed for the record id '.$id);
                        }
                    }
                }
            //Send mail without cron end

            
            if (count($_FILES)) {
                if ($warningCount) {
                    //$this->session->set_flashdata('type', 'error');
                    //$this->session->set_flashdata('message', 'Unable to send mail.' . $errors);
                } else {
                    
                        if ($form->auto_reply == 1) {
                            $get_email_field = ZestardContactField::where('form_id' , $form->form_id)->where('email_sender', 1)->first();
                            if(count($get_email_field) > 0)
                            {
                                $autoreply_sender = $form->auto_reply_sender;
                                $autoreply_receiver = $data['sender'];
                                $autoreply_message = $form->auto_reply_message;
                                $autoreply_subject = $form->auto_reply_subject;
                                
                                Mail::raw([], function ($message) use($autoreply_sender,$autoreply_receiver,$autoreply_subject,$autoreply_message,$shop) {
                                    $message->from($autoreply_sender,$shop);
                                    $message->to($autoreply_receiver)->subject($autoreply_subject);
                                    $message->setBody($autoreply_message, 'text/html');
                                });
                                
                            }                                
                        }
                        //$this->session->set_flashdata('type', 'success');
                        //$this->session->set_flashdata('message', 'Your mail has been successfully sent.');
                     
                }
            } else {
                
                    if ($form->auto_reply == 1) {
                            $get_email_field = ZestardContactField::where('form_id' , $form->form_id)->where('email_sender', 1)->first();
                            if(count($get_email_field) > 0)
                            {
                                $autoreply_sender = $form->auto_reply_sender;
                                $autoreply_receiver = $data['sender'];
                                $autoreply_message = $form->auto_reply_message;
                                $autoreply_subject = $form->auto_reply_subject;
                                
                                Mail::raw([], function ($message) use($autoreply_sender,$autoreply_receiver,$autoreply_subject,$autoreply_message,$shop) {
                                    $message->from($autoreply_sender,$shop);
                                    $message->to($autoreply_receiver)->subject($autoreply_subject);
                                    $message->setBody($autoreply_message, 'text/html');
                                });
                                
                            }                                
                        }
                
            }

        } catch (Exception $e) {
            //$this->session->set_flashdata('type', 'error');
            //$this->session->set_flashdata('message', $e->getMessage());
        }
        $url = 'https://' . $shop . '/pages/thank-you';
        header("location:".$url);
        //return redirect()->back();
    }

}
