<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;
use App\ShopModel;
use App\ZestardGlobalConfig;
class GlobalconfigController extends Controller
{
    public function index(request $request){
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $global_configuration = ZestardGlobalConfig::where('store_id' , $shop_find->id)->first();
        if(count($global_configuration) > 0)
        {
            return view('global_configuration',['data'=>$global_configuration]);
        }        
    	return view('global_configuration',['data'=>$global_configuration]);  
    }
    
    public function store(request $request){
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $global_configuration = ZestardGlobalConfig::where('store_id' , $shop_find->id)->first();
        $request['store_id'] = $shop_find->id;
        $request['date_field_order'] = serialize($request['date_field_order']);
        $request['year_range'] = $request['year_range_from'] .'-'.$request['year_range_to'];
        if(count($global_configuration) > 0){
            $global_configuration->update($request->all());
        }
        $insert_config = ZestardGlobalConfig::create($request->all());        
        return redirect('gobal_configuration');
        //return view('global_configuration',['data'=>$global_configuration]);  	    	
    }
}
