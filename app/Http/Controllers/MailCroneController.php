<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Log;
use App\AddNewsForm;
use App;
use DB;
use App\ShopModel;
use App\ZestardContactForm;
use App\ZestardContactField;
use App\EmailSend;
use Mail;

class MailCroneController extends Controller
{
    public function initiatecron(request $request){
        $email_records = EmailSend::where('email_status' , 0)->limit(5)->get();
       
        if(count($email_records) > 0){
            foreach ($email_records as $email_data)
            {
                $body = $email_data->email_template;
                $sender = $email_data->sender;
                $receiver = $email_data->receiver;
                $subject = $email_data->subject;
                $attachment = $email_data->attachment_path;
                $store_id = $email_data->store_id;
                $shop_find = ShopModel::where('id' , $store_id)->first();
                $shop = $shop_find->store_name;
                $temp_array = (array)json_decode($attachment);
                $id = $email_data->id;
                $multiple_receiver = explode(",",$receiver);
                try {
                    Mail::raw([], function ($message) use($temp_array,$sender,$multiple_receiver,$subject,$shop,$body) {
                        $message->from($sender,$shop);
                        $message->to($multiple_receiver)->subject($subject);
                        foreach($temp_array as $key => $value){
                              $message->attach($value, [
                                    'as' => $key ]);                    
                        }
                        $message->setBody($body, 'text/html');
                    });

                    EmailSend::where('id' , $id)->update(['email_status' => 1]);
                    //Log::info('Mail Cron sended mail for the record id '.$id);

                }catch (\Exception $ex) {
                    // Debug via $ex->getMessage();
                    //Log::info('Mail Cron failed for the record id '.$id);
                }
            }
        }
        
    }
}
