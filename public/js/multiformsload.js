// start
var anywhere_short_code_id = [];
var base_path_multiform = "https://zestardshop.com/shopifyapp/anywhere_custom_forms/public/";
var store_name = Shopify.shop;
var zestard_acf = " ";

if (!window.jQuery) {
    //If Undefined or Not available, Then Load	
    (function() {
        var jscript = document.createElement("script");
        jscript.src = base_path_multiform + "js/3.2.1.jquery.min.js";
        jscript.type = 'text/javascript';
        jscript.async = false;

        document.getElementsByTagName('head')[0].append(jscript);

        jscript.onload = function() {
            //Assigning Jquery Object to Zestard_jq
            zestard_acf = window.jQuery;
            if (typeof jQuery.ui == 'undefined') {
                //If Undefined or Not available, Then Load				
                var script = document.createElement('script');
                script.type = "text/javascript";
                script.src = base_path_multiform + 'js/jquery-ui.js';
                document.getElementsByTagName('head')[0].append(script);
                script.onload = function() {
                    //Calling Function
                    anywhere_custom_form_data();
                }
            }
        };
    })();
} else {
    //Check if Jquery UI is Undefined or Not available 					
    if (typeof jQuery.ui == 'undefined') {
        //If Undefined or Not available, Then Load				
        var jscript = document.createElement("script");
        jscript.src = base_path_multiform + "js/jquery-ui.js";
        jscript.type = 'text/javascript';
        jscript.async = false;
        document.getElementsByTagName('head')[0].append(jscript);
        jscript.onload = function() {
            zestard_acf = window.jQuery;
            //Calling Function
            anywhere_custom_form_data();
        }
    } else {
        zestard_acf = window.jQuery;
        //Calling Function
        anywhere_custom_form_data();
    }
}

//

var date_script = document.createElement('script');
date_script.type = "text/javascript";
date_script.src = "https://zestardshop.com/shopifyapp/anywhere_custom_forms/public/js/datetimepicker-master/build/jquery.datetimepicker.full.min.js";
document.getElementsByTagName('head')[0].appendChild(date_script);

//zestard_acf(document).ready(function () {
//var id = [];
///var base_path_multiform = "https://shopifydev.anujdalal.com/dev_anywhere_custom_forms/public/";
//var store_name = Shopify.shop;
function anywhere_custom_form_data() {
    zestard_acf('.Zestard_multi_custom_form').each(function() {
        if (zestard_acf(this).attr('id') !== '' && zestard_acf(this).attr('id') !== undefined) {
            anywhere_short_code_id.push(zestard_acf(this).attr('id'));
        }
    });

    if (anywhere_short_code_id.length) {
        zestard_acf.ajax({
            type: "POST",
            url: base_path_multiform + "get_form_data",
            crossDomain: true,
            data: {
                'id': anywhere_short_code_id,
                'shop': store_name
            },
            success: function(response) {
                var data = JSON.parse(response);
                var i = 0;
                zestard_acf(data).each(function(i) {
                    //console.log(data[i].html);
                    zestard_acf("#" + data[i].form_encryption_id).html(data[i].html);
                    i++;
                });
            }
        });
    }
}

//});