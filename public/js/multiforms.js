$(document).ready(function() {
    var globalFields;
    globalFields = $('.option-box').length;
    var hiddenFields;
    $('.add_field').click(function() {
        globalFields++;
        var newhtml = formCreator(globalFields);
        $('.panel-body.fieldsplayground').prepend(newhtml);
    });

    $('button.submitform').click(function() {
        $('.form-list:hidden').remove();
        $('tr:hidden').remove();
    });


    function formCreator(globalFields) {
        var htmlformdata;
        htmlformdata = '';
        htmlformdata += '<div class="table-responsive option-box info" id="field_' + globalFields + '">';
        htmlformdata += '<table id="contact_field" class="option-header table info" cellpadding="0" cellspacing="0">';
        htmlformdata += '<thead><tr class="info"><th class="opt-title">Title <span class="required">*</span></th><th class="opt-type">Input Type <span class="required">*</span></th><th class="opt-req">Is Required</th><th class="opt-order">Sort Order<div class="help-tip"><p>Enter the numeric value.</p></div></th>';
        htmlformdata += '<th class="a-right"></th>';
        htmlformdata += '</tr></thead><tbody><tr class="info"><td>';
        htmlformdata += '<input type="text" class="form-control required-entry" id="contact_field_' + globalFields + '_title" name="contact[fields][' + globalFields + '][title]"></td><td>';
        htmlformdata += '<select name="contact[fields][' + globalFields + '][type]" id="contact_field_' + globalFields + '_type" class="form-control required-entry" onchange="choosoption(this,' + globalFields + ')" title="">';
        htmlformdata += '<option value="">-- Please select --</option><optgroup label="Text"><option value="field">Field</option><option value="area">Area</option></optgroup>';
        htmlformdata += '<optgroup label="File"><option value="file">File</option></optgroup><optgroup label="Select"><option value="drop_down">Drop-down</option><option value="radio">Radio Buttons</option><option value="checkbox">Checkbox</option><option value="multiple">Multiple Select</option></optgroup>';
        htmlformdata += '<optgroup label="Date"><option value="date">Date</option><option value="date_time">Date &amp; Time</option><option value="time">Time</option></optgroup></select></td><td class="opt-req">';
        htmlformdata += '<select name="contact[fields][' + globalFields + '][is_require]" id="contact_field_' + globalFields + '_is_require" class="form-control" title="">';
        htmlformdata += '<option value="1">Yes</option><option value="0">No</option></select></td><td><input type="number" class="form-control required-entry" name="contact[fields][' + globalFields + '][sort_order]" value="" min="1">';
        htmlformdata += '</td><td><button id="" title="Delete Option" type="button" class="scalable delete btn btn-danger" onclick="deleteField(' + globalFields + ')"><span><i class="glyphicon glyphicon-trash"></i></span></button></td></tr></tbody></table></div>';
        return htmlformdata;
    }
});

function deleteField(fieldval) {
    var fieldid = $('.fieldid' + fieldval).val();
    if (fieldid != undefined) {
        var currVal = $("#deletefieldsids").val();
        if (currVal != '') {
            currVal = currVal.split(",");
            currVal.push(fieldid);
            currVal.join(",");
            $("#deletefieldsids").val(currVal);
        } else {
            $("#deletefieldsids").val(fieldid);
        }
    }
    $('#field_' + fieldval).remove();
}
/****************************SUB-FIELDS************************************/
function choosoption(selctedval, fieldval) {
    var option = $(selctedval).find('option:selected').val();
    if (option == 'field' || option == 'area') {
        typetextorarea(fieldval);
    } else if (option == 'file') {
        typefile(fieldval);
    } else if (option == 'drop_down' || option == 'radio' || option == 'checkbox' || option == 'multiple') {
        typeselect(fieldval);
    } else if (option == 'date_time' || option == 'time' || option == 'date') {
        typedatetime(fieldval);
    }
}

function typetextorarea(fieldval) {
    var typetextorarea;
    typetextorarea = '';
    typetextorarea += '<div id="contact_field_' + fieldval + '_type_text" class="table-responsive  grid tier col-sm-12 form-list">';
    typetextorarea += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="info headings"><th class="type-validation">Validation</th><th class="type-last">Max Characters<div class="help-tip"><p>Enter the numeric value.</p></div></th><th class="type-last last">Set as Email Sender<div class="help-tip"><p>if you select this it will set this field as sender of email.</p></div></th></tr><tr class="info"><td>';
    typetextorarea += '<select name="contact[fields][' + fieldval + '][validation]" id="contact_field_' + fieldval + '_validation" class="select form-control" title=""><option value="no">No</option><option value="email">Email</option></select></td><td class="type-last last">';
    typetextorarea += '<input type="number" class="form-control validate-zero-or-greater required-entry" name="contact[fields][' + fieldval + '][max_characters]" value="" min="1"></td><td><input type="radio" class="emailsender" name="contact[fields][' + fieldval + '][email_sender]"></td></tr></tbody></table></div>';
    $('#contact_field_' + fieldval + '_type_file').hide();
    $('#contact_field_' + fieldval + '_type_select').hide();
    if (!$('#contact_field_' + fieldval + '_type_text').length) {
        $('#field_' + fieldval).append(typetextorarea);
    } else {
        $('#contact_field_' + fieldval + '_type_text').show();
    }
}

function typefile(fieldval) {
    var typefile;
    typefile = '';
    typefile += '<div id="contact_field_' + fieldval + '_type_file" class="table-responsive col-sm-12 grid tier form-list">';
    typefile += '<table class="table border" cellpadding="0" cellspacing="0"><tbody><tr class="headings info"><th class="type-title">Allowed File Extensions<div class="help-tip"><p>Enter the flie extenstion which you want to allow with comma seperated like(jpg,png).</p></div></th><th class="last">Maximum File Size<div class="help-tip"><p>Enter the numeric value.</p></div></th></tr><tr class="info"><td><input class="form-control" type="text" name="contact[fields][' + fieldval + '][file_extension]" value="" ></td><td class="type-last last" nowrap=""><input class="form-control" type="number" name="contact[fields][' + fieldval + '][file_size]" min="1"> MB</td></tr></tbody></table></div>';
    $('#contact_field_' + fieldval + '_type_text').hide();
    $('#contact_field_' + fieldval + '_type_select').hide();
    if (!$('#contact_field_' + fieldval + '_type_file').length) {
        $('#field_' + fieldval).append(typefile);
    } else {
        $('#contact_field_' + fieldval + '_type_file').show();
    }
}

function typeselect(fieldval) {
    var typeselect;
    typeselect = '';
    typeselect += '<div id="contact_field_' + fieldval + '_type_select" class="grid col-sm-12 tier form-list">';
    typeselect += '<table class="table border" cellpadding="0" cellspacing="0">';
    typeselect += '<thead><tr class="tr_info headings"><th class="type-title">Title<span class="">*</span></th>';
    typeselect += '<th class="type-butt last">&nbsp;</th></tr></thead><tbody id="select_field_type_row_' + fieldval + '">';

    //Vj-start
    typeselect += '<tr class="subchildrows" id="contact_field_' + fieldval + '_select_' + fieldval + '"><td><input type="text" class="form-control required-entry input-text select-type-title" id="contact_field_' + fieldval + '_select_' + fieldval + '_title" name="contact[fields][' + fieldval + '][values][' + fieldval + '][title]" value=""></td><td class="last"></td></tr>';
    ////Vj-end


    typeselect += '</tbody><tfoot><tr class="tr_info">';
    typeselect += '<td colspan="100" class="a-right"><button id="add_select_row_button_' + fieldval + '" title="Add New Row" type="button" class="scalable add add-select-row btn btn-primary" onclick="addrow(' + fieldval + ')" style="float:right;"><span><span><span class="glyphicon glyphicon-plus"></span></span></span></button></td>';
    typeselect += '</tr></tfoot></table></div>';
    $('#contact_field_' + fieldval + '_type_text').hide();
    $('#contact_field_' + fieldval + '_type_video').hide();
    $('#contact_field_' + fieldval + '_type_file').hide();
    $('#contact_field_' + fieldval + '_type_textarea').hide();
    if (!$('#contact_field_' + fieldval + '_type_select').length) {
        $('#field_' + fieldval).append(typeselect);
    } else {
        $('#contact_field_' + fieldval + '_type_select').show();
    }
}

function addrow(fieldval) {
    var childFields;
    childFields = $('#select_field_type_row_' + fieldval + ' > .subchildrows').length;
    childFields++;
    var addrow;
    addrow = '';
    addrow += '<tr class="subchildrows" id="contact_field_' + fieldval + '_select_' + childFields + '">';
    addrow += '<td>';
    addrow += '<input type="text" class="required-entry form-control select-type-title" id="contact_field_' + fieldval + '_select_' + childFields + '_title" name="contact[fields][' + fieldval + '][values][' + childFields + '][title]" value=""></td>';
    addrow += '<td class="last"><span title="Delete row"><button id="id_' + childFields + '" title="Delete Row" type="button" class="scalable delete delete-select-row btn btn-warning icon-btn" onclick="removeRow(' + childFields + ',' + fieldval + ')" style=""><span><span><span><i class="glyphicon glyphicon-remove"></i></span></span></span></button></span></td></tr>';
    $('#select_field_type_row_' + fieldval).append(addrow);

}

function typedatetime(fieldval) {
    $('#contact_field_' + fieldval + '_type_text').hide();
    $('#contact_field_' + fieldval + '_type_file').hide();
    $('#contact_field_' + fieldval + '_type_select').hide();
}

function removeRow(childFields, fieldval) {
    $('#contact_field_' + fieldval + '_select_' + childFields).hide();
}

/**************************************************************************/